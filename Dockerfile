FROM eclipse-temurin:21 AS builder

WORKDIR /deployments

RUN apt-get update && apt-get install -y git 

COPY ./gradlew /deployments/
COPY ./gradle /deployments/gradle
RUN ./gradlew --version # cache the gradle wrapper

COPY ./ /deployments
RUN ./gradlew -x test assemble
RUN cp build/libs/pv-`./gradlew properties --no-daemon --console=plain -q | grep '^version:' | awk '{printf $2}'`.jar /deployments/pv.jar

FROM ghcr.io/graalvm/graalvm-community:21 AS runner
COPY --from=builder /deployments/pv.jar .
EXPOSE 8080
ENTRYPOINT exec java $JAVA_OPTS -jar pv.jar
