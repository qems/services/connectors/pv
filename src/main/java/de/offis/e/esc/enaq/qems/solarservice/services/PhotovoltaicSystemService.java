package de.offis.e.esc.enaq.qems.solarservice.services;

import de.offis.e.esc.enaq.qems.solarservice.models.devices.connectors.DeviceConnectorInfo;
import de.offis.e.esc.enaq.qems.solarservice.models.devices.connectors.DummyDeviceConnectorInfo;
import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * Interface for the Solar Service. The Solar Service is responsible for reading and writing to the model.
 *
 * @author Christian Pieper
 */
public interface PhotovoltaicSystemService {

    /**
     * Get the Solar System object by the appropriate ID
     *
     * @param photovoltaicSystemId The ID of the Solar System
     * @return Solar System object
     */
    Mono<PhotovoltaicSystem> getPhotovoltaicSystemById(String photovoltaicSystemId);

    /**
     * Retrieve a list of IDs of all available Solar Systems
     * @return List of IDs
     */
    Mono<List<String>> getListOfPhotovoltaicSystemIds();

    /**
     * Retrieve a list of all available Solar Systems
     *
     * @return List of SolarSystems
     */
    Flux<PhotovoltaicSystem> getListOfPhotovoltaicSystems();

    /**
     * Adds a SolarSystem.
     *
     * @param photovoltaicSystem The SolarSystem to add
     * @return An object of the added SolarSystem. It is important to use this object, as the ID can change during the
     * creation process.
     */
    Mono<PhotovoltaicSystem> addPhotovoltaicSystem(PhotovoltaicSystem photovoltaicSystem);

    /**
     * Removes a Solar System
     *
     * @param PhotovoltaicSystemId The id of the Solar System which will be removed.
     */
    Mono<Void> removePhotovoltaicSystem(String PhotovoltaicSystemId);

    /**
     * Updates an already persistent Solar System.
     *
     * @param solar Edited solar system model
     */
    Mono<PhotovoltaicSystem> updatePhotovoltaicSystem(PhotovoltaicSystem solar);

    /**
     * Updates an already persistent Solar System or adds a new Solar System if its ID does not exist
     *
     * @param solar Edited or created solar system model
     */
    Mono<PhotovoltaicSystem> addOrUpdatePhotovoltaicSystem(PhotovoltaicSystem solar);
    /**
     * Get information about all connectors
     * @return List of connectors
     */
    Flux<DeviceConnectorInfo> getConnectors();

    /**
     * Get information about all dummy connectors
     * @return List of dummy connectors
     */
    Flux<DummyDeviceConnectorInfo> getDummyDeviceConnectorInfo();

    /**
     * Get info about a specific connector. Error if the connector does not exist
     *
     * @param photovoltaicSystemId Solar system id that identifies the connector
     * @return Connector or error if it does not exist
     */
    Mono<DummyDeviceConnectorInfo> getDummyDeviceConnectorInfo(String photovoltaicSystemId);

    /**
     * Add a new or update an existing dummy connector
     * @param info Info about the added/updated connector
     * @return Connector with new information
     */
    Mono<DummyDeviceConnectorInfo> addOrUpdateDummyDeviceConnectorInfo(DummyDeviceConnectorInfo info);

    /**
     * Add a new dummy connector. Will fail if connector already exists
     * @param info Info about the added connector
     * @return New connector
     */
    Mono<DummyDeviceConnectorInfo> addDummyDeviceConnectorInfo(DummyDeviceConnectorInfo info);

    /**
     * Delete a dummy connector with the specified solar system id. Error if connector does not exist.
     *
     * @param photovoltaicSystemId Solar system id of the connector.
     * @return None. Error if connector does not exist.
     */
    Mono<Void> deleteDummyDeviceConnectorInfo(String photovoltaicSystemId);
}
