package de.offis.e.esc.enaq.qems.solarservice.events;

import org.springframework.context.ApplicationEvent;
import org.springframework.core.ResolvableType;
import org.springframework.core.ResolvableTypeProvider;

/**
 * @author Alexander Hill
 */
public class RemoveItemEvent<T> extends ApplicationEvent implements ResolvableTypeProvider {
    public final String itemId;
    private final Class<T> clazz;
    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param source the object on which the event initially occurred or with
     *               which the event is associated (never {@code null})
     */
    public RemoveItemEvent(Object source, String itemId, Class<T> clazz) {
        super(source);
        this.itemId = itemId;
        this.clazz = clazz;
    }

    @Override
    public ResolvableType getResolvableType() {
        return ResolvableType.forClassWithGenerics(
                getClass(),
                ResolvableType.forClass(clazz)
        );
    }
}
