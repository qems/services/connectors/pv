package de.offis.e.esc.enaq.qems.solarservice.api;

import de.offis.e.esc.enaq.qems.solarservice.exceptions.SolarSystemCreationFailedException;
import de.offis.e.esc.enaq.qems.solarservice.exceptions.SolarSystemLogNotFoundException;
import de.offis.e.esc.enaq.qems.solarservice.exceptions.SolarSystemNotFoundException;
import de.offis.e.esc.enaq.qems.solarservice.models.PhotovoltaicSystem.dtos.PhotovoltaicSystemDto;
import de.offis.e.esc.enaq.qems.solarservice.services.PhotovoltaicSystemService;
import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;
import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;
import java.util.TreeMap;

@RestController
@Validated
public class PhotovoltaicSystemController {

    private final PhotovoltaicSystemService photovoltaicSystemService;
    private final Logger logger = LoggerFactory.getLogger(PhotovoltaicSystemController.class);

    public PhotovoltaicSystemController(PhotovoltaicSystemService photovoltaicSystemService) {
        this.photovoltaicSystemService = photovoltaicSystemService;
    }

    private PhotovoltaicSystem transferDtoToModel(PhotovoltaicSystemDto photovoltaicSystemDto) {
        return new PhotovoltaicSystem(photovoltaicSystemDto.getId(), new TreeMap<>(), photovoltaicSystemDto.getMaxElectricalPowerOutputKw(),
                photovoltaicSystemDto.getStringsPerInverter(), photovoltaicSystemDto.getModulesPerString(),
                photovoltaicSystemDto.getModuleName(), photovoltaicSystemDto.getInverterName(),
                photovoltaicSystemDto.getLatitude(), photovoltaicSystemDto.getLongitude(),
                photovoltaicSystemDto.getAltitude());
    }

    /**
     * HTTP GET request - Returns a list of IDs of all Solar Systems
     *
     * @return ID list of available solar systems
     */
    @Operation(summary = "Get the list of solar system ids.")
    @GetMapping("/pv/ids")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Mono<List<String>> getSolarSystemIds() {
        logger.info("Process: GET '/solarsystems/ids'");
        return photovoltaicSystemService.getListOfPhotovoltaicSystemIds();
    }

    /**
     * HTTP GET request - Returns a list of all Solar Systems
     *
     * @return list of available solar systems
     */
    @Operation(summary = "Get the list of solar systems.")
    @GetMapping("/pv")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Flux<PhotovoltaicSystemDto> getSolarSystems() {
        logger.info("Process: GET '/pv'");
        return photovoltaicSystemService.getListOfPhotovoltaicSystems().map(PhotovoltaicSystemDto::new);
    }

    /**
     * HTTP POST request - Persists a new solar system
     *
     * @param photovoltaicSystemDto Raw data from view
     * @return The created solar system
     */
    @Operation(summary = "Add a new solar system.")
    @PostMapping("/pv")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Mono<PhotovoltaicSystemDto> persistNewSolarSystem(@Valid @RequestBody PhotovoltaicSystemDto photovoltaicSystemDto) {
        logger.info("Process: POST '/pvs'");

        PhotovoltaicSystem solarSystem = transferDtoToModel(photovoltaicSystemDto);
        return photovoltaicSystemService.addPhotovoltaicSystem(solarSystem)
                .map(PhotovoltaicSystemDto::new)
                .switchIfEmpty(Mono.error(new SolarSystemCreationFailedException(
                        photovoltaicSystemDto.getId(), photovoltaicSystemDto.getName())));
    }

    /**
     * HTTP PUT request - Updates a solar system if available or creates a new one, otherwise
     *
     * @param photovoltaicSystemDto Raw data from view
     * @return The updated or created solar system
     */
    @Operation(summary = "Add a new solar system or update an existing one.")
    @PutMapping("/pv")
    @ResponseBody
    public Mono<ResponseEntity<PhotovoltaicSystemDto>> addOrUpdateSolarSystem(@RequestBody PhotovoltaicSystemDto photovoltaicSystemDto) {
        logger.info("Process: PUT '/pv'");
        logger.info("Body: " + photovoltaicSystemDto);

        PhotovoltaicSystem solarSystem = transferDtoToModel(photovoltaicSystemDto);

        return photovoltaicSystemService.getPhotovoltaicSystemById(solarSystem.getId()).flatMap(photovoltaicSystemService::updatePhotovoltaicSystem)      // HTTP 200 OK -> given solar system exists, do an update
                .map(solarSystem1 -> new ResponseEntity<>(new PhotovoltaicSystemDto(solarSystem1), HttpStatus.OK))
                .switchIfEmpty(photovoltaicSystemService.addPhotovoltaicSystem(solarSystem)
                        // HTTP 201 CREATED ->  given solar system does not exist, create it
                        .map(solarSystem1 -> new ResponseEntity<>(new PhotovoltaicSystemDto(solarSystem1), HttpStatus.CREATED)));
    }

    /**
     * HTTP GET request - Returns a specific solar system
     *
     * @return Details about a specific solar systems
     */
    @Operation(summary = "Get a solar system by its id.")
    @GetMapping("/pv/{solarSystemId}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Mono<PhotovoltaicSystemDto> getPhotovoltaicSystem(@PathVariable String photovoltaicSystemId) {
        logger.info("Process: GET '/pv/{}'", photovoltaicSystemId);
        return photovoltaicSystemService.getPhotovoltaicSystemById(photovoltaicSystemId)
                .map(PhotovoltaicSystemDto::new)
                .switchIfEmpty(Mono.error(new SolarSystemNotFoundException(photovoltaicSystemId)));
    }

    /**
     * HTTP DELETE request - Deletes a specific solar system
     *
     * @param solarSystemId Solar system to delete
     */
    @Operation(summary = "Delete a solar system by its id.")
    @DeleteMapping("/pv/{solarSystemId}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<Void> deleteSolarSystem(@PathVariable @Min(0) String solarSystemId) {
        logger.info("Process: DEL /pv/{}", solarSystemId);
        return this.photovoltaicSystemService.removePhotovoltaicSystem(solarSystemId);
    }

    @ResponseStatus(
            value = HttpStatus.NOT_FOUND,
            reason = "Element not found")
    @ExceptionHandler({NullPointerException.class,
            SolarSystemNotFoundException.class,
            SolarSystemLogNotFoundException.class})
    public void nullPointerException() {}

    @ResponseStatus(
            value = HttpStatus.BAD_REQUEST,
            reason = "Illegal arguments")
    @ExceptionHandler(SolarSystemCreationFailedException.class)
    public void solarSystemCreationFailedException() {}

}
