package de.offis.e.esc.enaq.qems.solarservice.exceptions;

//@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Given Solar System ID could not be found.")
public class SolarSystemNotFoundException extends RuntimeException {
    public SolarSystemNotFoundException(String id) {
        super(String.format("Cannot find solar system (ID: %s)", id));
    }
}
