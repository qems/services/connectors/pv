package de.offis.e.esc.enaq.qems.solarservice.api;

import de.offis.e.esc.enaq.qems.solarservice.exceptions.SolarSystemConnectorNotFoundException;
import de.offis.e.esc.enaq.qems.solarservice.exceptions.SolarSystemLogNotFoundException;
import de.offis.e.esc.enaq.qems.solarservice.exceptions.SolarSystemNotFoundException;
import de.offis.e.esc.enaq.qems.solarservice.models.devices.connectors.DummyDeviceConnectorInfo;
import de.offis.e.esc.enaq.qems.solarservice.models.devices.connectors.dtos.DummyDeviceConnectorInfoDto;
import de.offis.e.esc.enaq.qems.solarservice.services.PhotovoltaicSystemService;
import io.swagger.v3.oas.annotations.Operation;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.management.InstanceAlreadyExistsException;

/**
 * REST controller for adding, updating and removing dummy connector information
 * @author Alexander Hill
 */
@RestController
@Validated
@RequestMapping("connectors/dummy")
public class DummyConnectorInfoController {
    private final Logger logger = LoggerFactory.getLogger(DummyConnectorInfoController.class);

    @Autowired
    private PhotovoltaicSystemService photovoltaicSystemService;
    private final ModelMapper mapper;

    public DummyConnectorInfoController(ModelMapper mapper) {
        this.mapper = mapper;
    }

    /**
     * HTTP GET request - Returns a list of all dummy device connectors
     *
     * @return list of available device connectors
     */
    @Operation(summary = "Get the list of Dummy device connectors.")
    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Flux<DummyDeviceConnectorInfoDto> getDummyDeviceConnectorInfoFlux() {
        logger.info("Process: GET '/connector/dummy'");
        return photovoltaicSystemService.getDummyDeviceConnectorInfo()
                .map(savedInfo -> mapper.map(savedInfo, DummyDeviceConnectorInfoDto.class));
    }

    /**
     * HTTP GET request - Returns the requested dummy device connector
     * @param solarSystemId Id of the solar system that connects to the service
     * @return device connector
     */
    @Operation(summary = "Get the list of dummy device connectors.")
    @GetMapping("/{solarSystemId}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Mono<DummyDeviceConnectorInfoDto> getDummyDeviceConnectorInfo(@PathVariable String solarSystemId) {
        logger.info("Process: GET '/connector/dummy/{}'", solarSystemId);
        return photovoltaicSystemService.getDummyDeviceConnectorInfo(solarSystemId)
                .switchIfEmpty(Mono.error(new SolarSystemConnectorNotFoundException(solarSystemId)))
                .map(savedInfo -> mapper.map(savedInfo, DummyDeviceConnectorInfoDto.class));
    }

    /**
     * HTTP PUT request - Add or update the connector info for a device
     *
     * @param info Connector info
     * @return Saved device connector
     */
    @Operation(summary = "Get the list of Dummy device connectors.")
    @PutMapping("")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Mono<DummyDeviceConnectorInfoDto> addOrUpdateDummyDeviceConnectorInfoMono(@RequestBody DummyDeviceConnectorInfoDto info) {
        logger.info("Process: PUT '/connector/dummy' with {}", info);

        return photovoltaicSystemService.addOrUpdateDummyDeviceConnectorInfo(mapper.map(info, DummyDeviceConnectorInfo.class))
                .map(savedInfo -> mapper.map(savedInfo, DummyDeviceConnectorInfoDto.class));
    }

    /**
     * HTTP POST request - Add the connector info for a device
     *
     * @param info Connector info
     * @return New device connector
     */
    @Operation(summary = "Get the list of dummy device connectors.")
    @PostMapping("")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Mono<DummyDeviceConnectorInfoDto> addDummyDeviceConnectorInfoMono(@RequestBody DummyDeviceConnectorInfoDto info) {
        logger.info("Process: POST '/connector/dummy' with {}", info);
        return photovoltaicSystemService.addDummyDeviceConnectorInfo(mapper.map(info, DummyDeviceConnectorInfo.class))
                .map(savedInfo -> mapper.map(savedInfo, DummyDeviceConnectorInfoDto.class));
    }

    /**
     * HTTP GET request - Returns the requested dummy device connector
     * @param solarSystemId Id of the requested solar system
     * @return device connector
     */
    @Operation(summary = "Get the list of dummy device connectors.")
    @DeleteMapping("/{solarSystemId}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Mono<Void> deleteDummyDeviceConnectorInfo(@PathVariable String solarSystemId) {
        logger.info("Process: DELETE '/connector/dummy/{}'", solarSystemId);
        return photovoltaicSystemService.deleteDummyDeviceConnectorInfo(solarSystemId);
    }

    @ResponseStatus(
            value = HttpStatus.NOT_FOUND,
            reason = "Element not found")
    @ExceptionHandler({NullPointerException.class,
            SolarSystemNotFoundException.class,
            SolarSystemConnectorNotFoundException.class,
            SolarSystemLogNotFoundException.class})
    public void nullPointerException() {}

    @ResponseStatus(
            value = HttpStatus.BAD_REQUEST,
            reason = "Element already exists")
    @ExceptionHandler({InstanceAlreadyExistsException.class})
    public void ElementExistsAlreadyException() {}

}
