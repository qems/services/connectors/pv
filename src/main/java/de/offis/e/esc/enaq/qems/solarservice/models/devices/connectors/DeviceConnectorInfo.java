package de.offis.e.esc.enaq.qems.solarservice.models.devices.connectors;

import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;

/**
 * Basic device connector information to store in the database. Only derived classes should be saved into a database.
 * @author Alexander Hill
 */
public abstract class DeviceConnectorInfo {
    @Id
    private String photovoltaicSystemId;
    @Transient
    private PhotovoltaicSystem photovoltaicSystem;

    public String getPhotovoltaicSystemId() {
        return photovoltaicSystemId;
    }

    public void setPhotovoltaicSystemId(String photovoltaicSystemId) {
        this.photovoltaicSystemId = photovoltaicSystemId;
    }

    public PhotovoltaicSystem getPhotovoltaicSystem() {
        return photovoltaicSystem;
    }

    public void setPhotovoltaicSystem(PhotovoltaicSystem photovoltaicSystem) {
        this.photovoltaicSystem = photovoltaicSystem;
    }
}
