package de.offis.e.esc.enaq.qems.solarservice.services.forecast;

import de.offis.e.esc.enaq.qems.solarservice.models.UpdateReason;
import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;

/**
 * @author Alexander Hill
 */
public class PhotovoltaicSystemDTO {
    private double maxElectricalPowerOutputKw;

    private String moduleName;
    private String inverterName;
    private Long stringsPerInverter;
    private Long modulesPerString;
    private Double latitude;
    private Double longitude;
    private Double altitude;
    private String id;
    private String name;
    private String description;
    private UpdateReason updateReason;

    public PhotovoltaicSystemDTO(PhotovoltaicSystem photovoltaicSystem) {
        setId(photovoltaicSystem.getId());
        setMaxElectricalPowerOutputKw(photovoltaicSystem.getOutputElectricalMax() / 1000);
        setModuleName(photovoltaicSystem.getInformation().getModuleName());
        setInverterName(photovoltaicSystem.getInformation().getInverterName());
        setStringsPerInverter(photovoltaicSystem.getInformation().getStringsPerInverter());
        setModulesPerString(photovoltaicSystem.getInformation().getModulesPerString());
        setLatitude(photovoltaicSystem.getLocation().getLatitude());
        setLongitude(photovoltaicSystem.getLocation().getLongitude());
        setAltitude(photovoltaicSystem.getLocation().getAltitude());
    }

    public PhotovoltaicSystemDTO() {}

    public UpdateReason getUpdateReason() {
        return updateReason;
    }

    public void setUpdateReason(UpdateReason updateReason) {
        this.updateReason = updateReason;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getMaxElectricalPowerOutputKw() {
        return maxElectricalPowerOutputKw;
    }

    public void setMaxElectricalPowerOutputKw(double maxElectricalPowerOutputKw) {
        this.maxElectricalPowerOutputKw = maxElectricalPowerOutputKw;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getInverterName() {
        return inverterName;
    }

    public void setInverterName(String inverterName) {
        this.inverterName = inverterName;
    }

    public Long getStringsPerInverter() {
        return stringsPerInverter;
    }

    public void setStringsPerInverter(Long stringsPerInverter) {
        this.stringsPerInverter = stringsPerInverter;
    }

    public Long getModulesPerString() {
        return modulesPerString;
    }

    public void setModulesPerString(Long modulesPerString) {
        this.modulesPerString = modulesPerString;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    @Override
    public String toString() {
        return "SolarSystemDTO{" +
                "maxElectricalPowerOutputKw=" + maxElectricalPowerOutputKw +
                ", moduleName='" + moduleName + '\'' +
                ", inverterName='" + inverterName + '\'' +
                ", stringsPerInverter=" + stringsPerInverter +
                ", modulesPerString=" + modulesPerString +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", altitude=" + altitude +
                '}';
    }
}
