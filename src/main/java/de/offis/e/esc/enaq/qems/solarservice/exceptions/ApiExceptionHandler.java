package de.offis.e.esc.enaq.qems.solarservice.exceptions;

/**
 * Class to handle all kinds of exceptions in order to give proper responses.
 * It is based on a tutorial from https://www.baeldung.com/global-error-handler-in-a-spring-rest-api
 *
 * @author Christian Pieper
 */
/*
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {
    private Logger logger = LoggerFactory.getLogger(ApiExceptionHandler.class);

    // Handles @Validation errors
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {

        List<String> errors = new ArrayList<>();
        logger.info(request.getDescription(false));

        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }

        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors);
        return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
    }

    @ExceptionHandler({ SolarSystemNotFoundException.class })
    protected ResponseEntity<Object> handleSolarSystemNotFoundException(
            SolarSystemNotFoundException ex,
            WebRequest request) {

        logger.info(request.getDescription(false));
        String error = ex.getMessage();

        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({ SolarSystemLogNotFoundException.class })
    protected ResponseEntity<Object> handleSolarSystemLogNotFoundException(
            SolarSystemLogNotFoundException ex,
            WebRequest request) {

        logger.info(request.getDescription(false));
        String error = ex.getMessage();

        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({ SolarSystemCreationFailedException.class, SolarSystemUpdateFailedException.class })
    protected ResponseEntity<Object> handleSolarSystemPersistenceExceptions(
            RuntimeException ex,
            WebRequest request) {

        logger.info(request.getDescription(false));
        String error = ex.getMessage();

        ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public void constraintViolationException(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }

    // Fall-back handler
    @ExceptionHandler({ Exception.class })
    public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
        logger.info(request.getDescription(false));

        ApiError apiError = new ApiError(
                HttpStatus.INTERNAL_SERVER_ERROR, ex.getLocalizedMessage(), "error occurred");

        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }
}
*/
