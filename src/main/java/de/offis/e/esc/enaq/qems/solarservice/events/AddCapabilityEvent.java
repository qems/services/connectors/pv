package de.offis.e.esc.enaq.qems.solarservice.events;

import de.offis.e.esc.qems.api.entities.controls.Capability;
import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;
import org.springframework.context.ApplicationEvent;
import org.springframework.core.ResolvableType;
import org.springframework.core.ResolvableTypeProvider;

public class AddCapabilityEvent<T extends Capability> extends ApplicationEvent implements ResolvableTypeProvider {
    public final T capability;
    public final PhotovoltaicSystem entity;

    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param source the object on which the event initially occurred or with
     *               which the event is associated (never {@code null})
     * @param capability added capability
     * @param entity associated entity
     */
    public AddCapabilityEvent(Object source, T capability, PhotovoltaicSystem entity) {
        super(source);
        this.capability = capability;
        this.entity = entity;
    }

    @Override
    public ResolvableType getResolvableType() {
        return ResolvableType.forClassWithGenerics(
                getClass(),
                ResolvableType.forInstance(capability)
        );
    }
}
