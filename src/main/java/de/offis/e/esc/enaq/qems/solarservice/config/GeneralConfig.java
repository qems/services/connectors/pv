package de.offis.e.esc.enaq.qems.solarservice.config;

import de.offis.e.esc.enaq.qems.solarservice.models.service.ServiceConnectorInfo;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Alexander Hill
 * Miscantellous instance configurations for this solar service. Will set the connector info and model mapper configuration
 */
@Configuration
public class GeneralConfig {
    // When connection via rabbitmq is enabled, also add the right queue to this code
    @Bean
    public ServiceConnectorInfo getConnectorInfo(@Value("${connector.type:-REST}") String connectorType,
                                                 @Value("${server.port}") String port,
                                                 @Value("${service.name:#{null}}") String serviceName)
            throws Exception {
        if (connectorType.equals(ServiceConnectorInfo.ConnectorType.AMQP.name())) {
            return getAMQPConnectorInfo();
        }
        // Last resort is always REST interface
        return getRESTConnectorInfo(port, serviceName);
    }

    private ServiceConnectorInfo getRESTConnectorInfo(String port, String serviceName) throws UnknownHostException {
        InetAddress address = InetAddress.getLocalHost();
        String hostname = address.getHostName();
        String hostAddress = address.getHostAddress();
        ServiceConnectorInfo connectorInfo = new ServiceConnectorInfo();
        connectorInfo.connectorType = ServiceConnectorInfo.ConnectorType.REST;
        connectorInfo.properties.put("port", String.valueOf(port));
        connectorInfo.properties.put("ip", hostAddress);
        connectorInfo.properties.put("instanceid", UUID.randomUUID().toString());
        connectorInfo.properties.put("servicetype", "PV");
        Optional.ofNullable(serviceName).ifPresentOrElse(
                value -> {
                    connectorInfo.properties.put("hostname", hostname + "." + value);
                    connectorInfo.properties.put("homepageurl", "http://" + hostname + "." + value + ":" + port + "/");
                },
                () -> {
                    connectorInfo.properties.put("hostname", hostname);
                    connectorInfo.properties.put("homepageurl", "http://" + hostname + ":" + port + "/");
                }
        );
        return connectorInfo;
    }

    private ServiceConnectorInfo getAMQPConnectorInfo() throws Exception {
        ServiceConnectorInfo connectorInfo = new ServiceConnectorInfo();
        connectorInfo.connectorType = ServiceConnectorInfo.ConnectorType.AMQP;
        connectorInfo.properties.put("instanceid", UUID.randomUUID().toString());

        throw new Exception("AMQP is currently not supported. Use REST ConnectorInfo instead");
    }

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        return mapper;
    }
}
