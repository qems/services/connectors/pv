package de.offis.e.esc.enaq.qems.solarservice.api;

import de.offis.e.esc.enaq.qems.solarservice.exceptions.SolarSystemLogNotFoundException;
import de.offis.e.esc.enaq.qems.solarservice.exceptions.SolarSystemNotFoundException;
import de.offis.e.esc.enaq.qems.solarservice.models.devices.connectors.DeviceConnectorInfo;
import de.offis.e.esc.enaq.qems.solarservice.models.devices.connectors.dtos.DeviceConnectorInfoDto;
import de.offis.e.esc.enaq.qems.solarservice.services.PhotovoltaicSystemService;
import io.swagger.v3.oas.annotations.Operation;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.management.InstanceAlreadyExistsException;
import java.util.List;

/**
 * Defines and provides the REST endpoints for the solar system connectors
 * @author Jan-Henrik Bruhn
 */
@RestController
@RequestMapping("connectors")
@Validated
public class ConnectorInfoController {

    private final Logger logger = LoggerFactory.getLogger(ConnectorInfoController.class);
    @Autowired
    private PhotovoltaicSystemService photovoltaicSystemService;
    private final ModelMapper mapper;

    public ConnectorInfoController(ModelMapper mapper) {
        this.mapper = mapper;
    }


    /**
     * HTTP GET request - Returns a list of IDs of all solar system connectors
     *
     * @return ID list of available heat pump connectors
     */
    @Operation(summary = "Get the list of solar system connector ids.")
    @GetMapping("/ids")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Mono<List<String>> getSolarSystemConnectorIds() {
        logger.info("Process: GET '/connectors/ids'");
        return photovoltaicSystemService.getConnectors().map(DeviceConnectorInfo::getPhotovoltaicSystemId).collectList();
    }

    /**
     * HTTP GET request - Returns a list of all solar system connectors
     *
     * @return list of available heat pumps
     */
    @Operation(summary = "Get the list of solar system connectors.")
    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Flux<DeviceConnectorInfoDto> getConnectors() {
        logger.info("Process: GET '/connectors'");
        return photovoltaicSystemService.getConnectors().map(savedInfo -> mapper.map(savedInfo, DeviceConnectorInfoDto.class));
    }


    /**
     * HTTP GET request - Returns connectors for specific solar system
     *
     * @return Connectors for a specific solar system
     */
    @Operation(summary = "Get all connectors for a specific solar system.")
    @GetMapping("/{solarSystemId}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Flux<DeviceConnectorInfoDto> getSolarSystemConnectors(@PathVariable String solarSystemId) {
        logger.info("Process: GET '/connectors/{}'", solarSystemId);

        return photovoltaicSystemService.getPhotovoltaicSystemById(solarSystemId)
                .switchIfEmpty(Mono.error(new SolarSystemNotFoundException(solarSystemId)))
                .flatMapMany(solarSystem -> photovoltaicSystemService.getDummyDeviceConnectorInfo(solarSystem.getId())
                                        .cast(DeviceConnectorInfo.class)
                        ).map(savedInfo -> mapper.map(savedInfo, DeviceConnectorInfoDto.class));
    }


    @ResponseStatus(
            value = HttpStatus.NOT_FOUND,
            reason = "Element not found")
    @ExceptionHandler({NullPointerException.class,
            SolarSystemNotFoundException.class,
            SolarSystemLogNotFoundException.class})
    public void nullPointerException() {}

    @ResponseStatus(
            value = HttpStatus.BAD_REQUEST,
            reason = "Element already exists")
    @ExceptionHandler({InstanceAlreadyExistsException.class})
    public void ElementExistsAlreadyException() {}
}
