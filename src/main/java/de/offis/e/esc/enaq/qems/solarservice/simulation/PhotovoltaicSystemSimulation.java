package de.offis.e.esc.enaq.qems.solarservice.simulation;

import de.offis.mosaik.api.SimProcess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("simulation")
public class PhotovoltaicSystemSimulation extends Thread {
    private final Logger logger = LoggerFactory.getLogger(PhotovoltaicSystemSimulation.class);

    private final PhotovoltaicSystemSimulator simulator;

    private final String listenerAddress;
    private final int listenerPort;

    public PhotovoltaicSystemSimulation(PhotovoltaicSystemSimulator simulator, @Value("${mosaik.listener.port:4242}") int listenerPort, @Value("${mosaik.listener.address:0.0.0.0}") String listenerAddress) throws Exception {
        this.simulator = simulator;
        this.listenerPort = listenerPort;
        this.listenerAddress = listenerAddress;
        this.start();
    }

    @Override
    public void run() {
        while(true) {
            try {
                SimProcess.startSimulation(new String[]{this.listenerAddress + ":" + this.listenerPort, "server"}, simulator);
            } catch (Exception e) {
                logger.error("Simulation failed. Stopping simulation listener", e);
                System.exit(1);
                return;
            }
            logger.info("Simulation finished! Restarting Server");
        }
    }
}
