package de.offis.e.esc.enaq.qems.solarservice.database;

import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PhotovoltaicSystemRepository extends ReactiveMongoRepository<PhotovoltaicSystem, String> {
}
