package de.offis.e.esc.enaq.qems.solarservice.exceptions;

public class SolarSystemConnectorNotFoundException extends RuntimeException {
    public SolarSystemConnectorNotFoundException(String id) {
        super(String.format("Cannot find solar system connector (ID: %s)", id));
    }
}
