package de.offis.e.esc.enaq.qems.solarservice.exceptions;

public class SolarSystemUpdateFailedException extends RuntimeException {
    public SolarSystemUpdateFailedException(String errorMessage) {
        super(errorMessage);
    }

    public SolarSystemUpdateFailedException(int id, String solarSystemName) {
        super(String.format("Cannot update solar system (ID: %d, Name: %s)", id, solarSystemName));
    }
}
