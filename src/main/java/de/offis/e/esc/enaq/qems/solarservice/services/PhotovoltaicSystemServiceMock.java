package de.offis.e.esc.enaq.qems.solarservice.services;

import de.offis.e.esc.enaq.qems.solarservice.events.AddItemEvent;
import de.offis.e.esc.enaq.qems.solarservice.events.RemoveItemEvent;
import de.offis.e.esc.enaq.qems.solarservice.events.UpdateItemEvent;
import de.offis.e.esc.enaq.qems.solarservice.exceptions.SolarSystemNotFoundException;
import de.offis.e.esc.enaq.qems.solarservice.models.devices.connectors.DeviceConnectorInfo;
import de.offis.e.esc.enaq.qems.solarservice.models.devices.connectors.DummyDeviceConnectorInfo;
import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import javax.management.InstanceAlreadyExistsException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Profile({"test", "simulation"})
@Primary
@Service
public class PhotovoltaicSystemServiceMock implements PhotovoltaicSystemService {
    private final Logger logger = LoggerFactory.getLogger(PhotovoltaicSystemServiceMock.class);
    private final Map<String, PhotovoltaicSystem> photovoltaicSystemMap = new HashMap<>();
    private final Map<String, DummyDeviceConnectorInfo> dummyDeviceConnectorInfoMap = new HashMap<>();
    private final ApplicationEventPublisher applicationEventPublisher;

    public PhotovoltaicSystemServiceMock(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public Mono<PhotovoltaicSystem> getPhotovoltaicSystemById(String photovoltaicSystemId) {
        return Mono.justOrEmpty(photovoltaicSystemMap.get(photovoltaicSystemId)).switchIfEmpty(Mono.error(new SolarSystemNotFoundException("Solar system not found")));
    }

    @Override
    public Mono<List<String>> getListOfPhotovoltaicSystemIds() {
        Flux<PhotovoltaicSystem> photovoltaicSystemFlux = Flux.fromIterable(photovoltaicSystemMap.values());
        return photovoltaicSystemFlux.map(PhotovoltaicSystem::getId).collect(Collectors.toList());
    }

    @Override
    public Flux<PhotovoltaicSystem> getListOfPhotovoltaicSystems() {
        return Flux.fromIterable(photovoltaicSystemMap.values());
    }

    @Override
    public Mono<PhotovoltaicSystem> addPhotovoltaicSystem(PhotovoltaicSystem photovoltaicSystem) {
        if (photovoltaicSystem.getId() == null) {
            photovoltaicSystem.setId(UUID.randomUUID().toString());
        }
        photovoltaicSystemMap.put(photovoltaicSystem.getId(), photovoltaicSystem);
        // create new solar system
        return Mono.just(photovoltaicSystem)
                .map(solar -> { // establish connection to solar system
                    logger.info("Added solar system: {}", solar.getId());
                    applicationEventPublisher.publishEvent(new AddItemEvent<>(this, solar));
                    return solar;
                });
    }

    @Override
    public Mono<Void> removePhotovoltaicSystem(String PhotovoltaicSystemId) {
        return Mono.just(photovoltaicSystemMap.containsKey(PhotovoltaicSystemId))
                .flatMap(exists -> {
            if (!exists) {
                {
                    throw new SolarSystemNotFoundException(PhotovoltaicSystemId);
                }
            }
            applicationEventPublisher.publishEvent(new RemoveItemEvent<>(this, PhotovoltaicSystemId, PhotovoltaicSystem.class));
            logger.info("Removed solar system: {}", PhotovoltaicSystemId);
            photovoltaicSystemMap.remove(PhotovoltaicSystemId);
            dummyDeviceConnectorInfoMap.remove(PhotovoltaicSystemId);
            return Mono.empty();
        });
    }

    @Override
    public Mono<PhotovoltaicSystem> updatePhotovoltaicSystem(PhotovoltaicSystem photovoltaicSystem) {
        return this.getPhotovoltaicSystemById(photovoltaicSystem.getId()).map(photovoltaicSystem1 -> {
            updatePhotovoltaicSystem(photovoltaicSystem1, photovoltaicSystem);
            logger.info("Updated solar system: {}", photovoltaicSystem.getId());
            applicationEventPublisher.publishEvent(new UpdateItemEvent<>(this, photovoltaicSystem1));
            photovoltaicSystemMap.put(photovoltaicSystem1.getId(), photovoltaicSystem1);
            return photovoltaicSystem1;
        }).switchIfEmpty(Mono.error(new SolarSystemNotFoundException(photovoltaicSystem.getId())));
    }

    private void updatePhotovoltaicSystem(PhotovoltaicSystem savedPhotovoltaicSystem, PhotovoltaicSystem solarSystem) {
        savedPhotovoltaicSystem.setOutputElectricalMax(solarSystem.getOutputElectricalMax());
        savedPhotovoltaicSystem.setOutputElectricalMin(solarSystem.getOutputElectricalMin());
        savedPhotovoltaicSystem.setInformation(solarSystem.getInformation());
        savedPhotovoltaicSystem.setLocation(solarSystem.getLocation());
    }

    @Override
    public Mono<PhotovoltaicSystem> addOrUpdatePhotovoltaicSystem(PhotovoltaicSystem photovoltaicSystem) {
        return Mono.just(this.photovoltaicSystemMap.containsKey(photovoltaicSystem.getId()))
                .flatMap(exists -> {
                    if (exists) {
                        return this.updatePhotovoltaicSystem(photovoltaicSystem);
                    } else {
                        logger.warn("Cannot find solar system: {}", photovoltaicSystem.getId());
                        return this.addPhotovoltaicSystem(photovoltaicSystem);
                    }
                });
    }

    @Override
    public Flux<DeviceConnectorInfo> getConnectors() {
        return
                this.getDummyDeviceConnectorInfo().cast(DeviceConnectorInfo.class);
    }

    @Override
    public Flux<DummyDeviceConnectorInfo> getDummyDeviceConnectorInfo() {
        return Flux.fromIterable(dummyDeviceConnectorInfoMap.values());
    }

    @Override
    public Mono<DummyDeviceConnectorInfo> getDummyDeviceConnectorInfo(String photovoltaicSystemId) {
        return Mono.justOrEmpty(dummyDeviceConnectorInfoMap.get(photovoltaicSystemId));
    }

    @Override
    public Mono<DummyDeviceConnectorInfo> addOrUpdateDummyDeviceConnectorInfo(DummyDeviceConnectorInfo info) {
        return addOrUpdateDeviceConnectorInfo(info)
                .doOnSuccess(dummyDeviceConnectorInfo -> dummyDeviceConnectorInfoMap.put(dummyDeviceConnectorInfo.getPhotovoltaicSystemId(), dummyDeviceConnectorInfo)
                );
    }

    @Override
    public Mono<DummyDeviceConnectorInfo> addDummyDeviceConnectorInfo(DummyDeviceConnectorInfo info) {
        return addDeviceConnectorInfo(info)
                .doOnSuccess(dummyDeviceConnectorInfo ->
                        dummyDeviceConnectorInfoMap.put(dummyDeviceConnectorInfo.getPhotovoltaicSystemId(), dummyDeviceConnectorInfo));
    }

    @Override
    public Mono<Void> deleteDummyDeviceConnectorInfo(String photovoltaicSystemId) {
        return deleteDeviceConnectorInfo(photovoltaicSystemId, DummyDeviceConnectorInfo.class)
                .doOnSuccess(unused -> dummyDeviceConnectorInfoMap.remove(photovoltaicSystemId));
    }

    public <T extends DeviceConnectorInfo> Mono<T> addOrUpdateDeviceConnectorInfo(T info) {
        return getPhotovoltaicSystemById(info.getPhotovoltaicSystemId())
                .zipWith(Mono.just(info))
                .doOnNext(solarSystemConnectorTuple -> {
                    T savedInfo = solarSystemConnectorTuple.getT2();
                    savedInfo.setPhotovoltaicSystem(solarSystemConnectorTuple.getT1());
                    applicationEventPublisher.publishEvent(new AddItemEvent<>(this, savedInfo));
                })
                .map(Tuple2::getT2);
    }

    public <T extends DeviceConnectorInfo> Mono<T> addDeviceConnectorInfo(T info) {
        return getConnectors().filter(conn -> conn.getPhotovoltaicSystemId().equals(info.getPhotovoltaicSystemId()))
                .flatMap(connectorInfo -> Mono.<T>error(new InstanceAlreadyExistsException("Please remove the existing connector first!")))
                .then(this.addOrUpdateDeviceConnectorInfo(info));
    }

    public <T extends DeviceConnectorInfo> Mono<Void> deleteDeviceConnectorInfo(String solarSystemId, Class<T> clazz) {
        return getPhotovoltaicSystemById(solarSystemId).flatMap(solarSystem -> {
            applicationEventPublisher.publishEvent(new RemoveItemEvent<T>(this, solarSystemId, clazz));
            return null;
        });
    }
}
