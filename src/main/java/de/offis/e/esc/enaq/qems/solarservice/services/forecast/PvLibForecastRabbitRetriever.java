package de.offis.e.esc.enaq.qems.solarservice.services.forecast;

import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

/**
 * Container that retrieves data via AMQP RPC from the pv prediction service.
 * @author Alexander Hill
 */
@Component
@Profile("!test")
public class PvLibForecastRabbitRetriever implements PvLibForecastRetriever {

    private final Logger logger = LoggerFactory.getLogger(PvLibForecastRabbitRetriever.class);
    AmqpTemplate rabbitTemplate;
    String rpcExchange;
    String influxForecastBucket;

    @Autowired
    public PvLibForecastRabbitRetriever(AmqpTemplate rabbitTemplate,
                                        @Value("${forecasts.influx.bucket}") String influxForecastBucket,
                                        @Value("${rabbitmq.pv.prediction.rpc.exchange}") String rpcExchange) {
        this.rabbitTemplate = rabbitTemplate;
        this.rpcExchange = rpcExchange;
        this.influxForecastBucket = influxForecastBucket;
    }

    @Override
    public Mono<Map<Long, Double>> retrievePvForecastMono(PhotovoltaicSystem solarSystem, Instant timestampStart, Instant timestampStop, Instant timestampNow) {
        Mono<Map<Long, Double>> mono = Mono.fromCallable(() -> {
            try {
                Map<Long, Double> result = rabbitTemplate.convertSendAndReceiveAsType(
                        rpcExchange, "pvlib.satellite",
                        new PvLibForecastRequestDTO(new PhotovoltaicSystemDTO(solarSystem), influxForecastBucket, timestampStart.toEpochMilli() / 1000, timestampStop.toEpochMilli() / 1000, timestampNow.toEpochMilli() / 1000),
                        m -> m,
                        new ParameterizedTypeReference<Map<Long, Double>>() {
                        }
                );
                assert result != null;
                return result;//.entrySet().parallelStream().collect(Collectors.toMap(entry -> Instant.ofEpochMilli(entry.getKey()), Map.Entry::getValue));
            } catch (Exception e) {
                logger.error("Could not get PV Prediction data", e);
                return new HashMap<>();
            }
        });
        return mono.subscribeOn(Schedulers.boundedElastic());
    }

    @Override
     public void setBucket(String bucket) {
        this.influxForecastBucket = bucket;
     }

}
