package de.offis.e.esc.enaq.qems.solarservice.exceptions;

public class SolarSystemCreationFailedException extends RuntimeException {
    public SolarSystemCreationFailedException(String errorMessage) {
        super(errorMessage);
    }

    public SolarSystemCreationFailedException(String id, String solarSystemName) {
        super(String.format("Cannot create solar system (ID: %s, Name: %s)", id, solarSystemName));
    }
}
