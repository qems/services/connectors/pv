package de.offis.e.esc.enaq.qems.solarservice.services;

import de.offis.e.esc.enaq.qems.solarservice.database.DummyDeviceConnectorInfoRepository;
import de.offis.e.esc.enaq.qems.solarservice.database.PhotovoltaicSystemRepository;
import de.offis.e.esc.enaq.qems.solarservice.events.AddItemEvent;
import de.offis.e.esc.enaq.qems.solarservice.events.RemoveItemEvent;
import de.offis.e.esc.enaq.qems.solarservice.events.UpdateItemEvent;
import de.offis.e.esc.enaq.qems.solarservice.exceptions.SolarSystemNotFoundException;
import de.offis.e.esc.enaq.qems.solarservice.models.devices.connectors.DeviceConnectorInfo;
import de.offis.e.esc.enaq.qems.solarservice.models.devices.connectors.DummyDeviceConnectorInfo;
import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import javax.management.InstanceAlreadyExistsException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The main service to manage and control Solar Systems. It accesses and persists the data via Hibernate in a database.
 *
 * @author Christian Pieper
 */
@Profile({"!simulation & !test"})
@Service
public class PhotovoltaicSystemServiceDB implements PhotovoltaicSystemService {

    // Only keep the latest log per solar system
    private final Logger logger = LoggerFactory.getLogger(PhotovoltaicSystemServiceDB.class);
    private final PhotovoltaicSystemRepository photovoltaicSystemRepository;
    private final DummyDeviceConnectorInfoRepository dummyDeviceConnectorInfoRepository;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final Map<String, PhotovoltaicSystem> solarSystems = new HashMap<>();

    public PhotovoltaicSystemServiceDB(PhotovoltaicSystemRepository photovoltaicSystemRepository,
                                       DummyDeviceConnectorInfoRepository dummyDeviceConnectorInfoRepository,
                                       ApplicationEventPublisher applicationEventPublisher) {
        this.photovoltaicSystemRepository = photovoltaicSystemRepository;
        this.dummyDeviceConnectorInfoRepository = dummyDeviceConnectorInfoRepository;
        this.applicationEventPublisher = applicationEventPublisher;


    }
    @EventListener(ApplicationStartedEvent.class)
    private void initializeFromExistingSolarSystems() {
        photovoltaicSystemRepository.findAll().doOnNext(photovoltaicSystem -> {
            // Load and initialize all existing solar systems from the database
            solarSystems.put(photovoltaicSystem.getId(), photovoltaicSystem);
            applicationEventPublisher.publishEvent(new AddItemEvent<>(this, photovoltaicSystem));
        }).thenMany(
                // Then add all dummy connectors from the database
                getDummyDeviceConnectorInfo().flatMap(dummyDeviceConnectorInfo ->
                        // Set connection between connector and solar system
                        getPhotovoltaicSystemById(dummyDeviceConnectorInfo.getPhotovoltaicSystemId()).map(photovoltaicSystem -> {
                            dummyDeviceConnectorInfo.setPhotovoltaicSystem(photovoltaicSystem);
                            return dummyDeviceConnectorInfo;
                        })
                ).doOnNext(dummyDeviceConnectorInfo ->
                        // Event will publish and finalize the new connector
                        applicationEventPublisher.publishEvent(new AddItemEvent<>(this, dummyDeviceConnectorInfo))
                )).subscribe();
    }

    @Override
    public Mono<PhotovoltaicSystem> getPhotovoltaicSystemById(String photovoltaicSystemId) {
        return Mono.justOrEmpty(solarSystems.get(photovoltaicSystemId));
    }

    @Override
    public Mono<List<String>> getListOfPhotovoltaicSystemIds() {
        return Flux.fromIterable(solarSystems.keySet()).collectList();
    }

    @Override
    public Flux<PhotovoltaicSystem> getListOfPhotovoltaicSystems() {
        return Flux.fromIterable(solarSystems.values());
    }

    @Override
    public Mono<PhotovoltaicSystem> addPhotovoltaicSystem(PhotovoltaicSystem photovoltaicSystem) {
        // create new solar system
        Mono<PhotovoltaicSystem> createPhotovoltaicSystem = photovoltaicSystemRepository.save(photovoltaicSystem)
                .map(photovoltaicSystem1 -> { // establish connection to solar system
                    solarSystems.put(photovoltaicSystem1.getId(), photovoltaicSystem1);
                    logger.info("..added SolarSystem(id='{}') to database", photovoltaicSystem1.getId());
                    applicationEventPublisher.publishEvent(new AddItemEvent<>(this, photovoltaicSystem1));
                    return photovoltaicSystem1;
                });
        // Check first if a solarSystem with the name exists
        if (photovoltaicSystem.getId() == null) {
            return createPhotovoltaicSystem;
        }
        return getPhotovoltaicSystemById(photovoltaicSystem.getId())
                // If exists, get error
                .flatMap(photovoltaicSystemUnused -> Mono.<PhotovoltaicSystem>error(new InstanceAlreadyExistsException()))
                .switchIfEmpty(createPhotovoltaicSystem);
    }

    @Override
    public Mono<Void> removePhotovoltaicSystem(String PhotovoltaicSystemId) {
        return getPhotovoltaicSystemById(PhotovoltaicSystemId)
                .switchIfEmpty(Mono.error(new SolarSystemNotFoundException(PhotovoltaicSystemId)))
                .flatMap(exists -> {
                    logger.info("...remove SolarSystem(id='{}') from db", PhotovoltaicSystemId);
                    return photovoltaicSystemRepository.deleteById(PhotovoltaicSystemId)
                            .doOnSuccess(unused -> solarSystems.remove(PhotovoltaicSystemId))
                            .doOnSuccess(unused -> applicationEventPublisher.publishEvent(new RemoveItemEvent<>(this, PhotovoltaicSystemId, PhotovoltaicSystem.class)))
                            .doOnSuccess(unused -> logger.debug("...found SolarSystem(id='{}'), trying to delete it...", PhotovoltaicSystemId));

                });
    }
    @Override
    public Mono<PhotovoltaicSystem> updatePhotovoltaicSystem(PhotovoltaicSystem updatedPhotovoltaicSystem) {
        return getPhotovoltaicSystemById(updatedPhotovoltaicSystem.getId()).flatMap(photovoltaicSystem -> {
            updatePhotovoltaicSystem(photovoltaicSystem, updatedPhotovoltaicSystem);
            logger.info("Updated solar system: {} ", updatedPhotovoltaicSystem.getId());
            applicationEventPublisher.publishEvent(new UpdateItemEvent<>(this, photovoltaicSystem));
            return photovoltaicSystemRepository.save(photovoltaicSystem);
        }).switchIfEmpty(Mono.error(new SolarSystemNotFoundException(updatedPhotovoltaicSystem.getId())));
    }

    private void updatePhotovoltaicSystem(PhotovoltaicSystem savedPhotovoltaicSystem, PhotovoltaicSystem solarSystem) {
        savedPhotovoltaicSystem.setOutputElectricalMax(solarSystem.getOutputElectricalMax());
        savedPhotovoltaicSystem.setOutputElectricalMin(solarSystem.getOutputElectricalMin());
        savedPhotovoltaicSystem.setInformation(solarSystem.getInformation());
        savedPhotovoltaicSystem.setLocation(solarSystem.getLocation());
    }

    @Override
    public Mono<PhotovoltaicSystem> addOrUpdatePhotovoltaicSystem(PhotovoltaicSystem solar) {
        return this.photovoltaicSystemRepository.existsById(solar.getId())
                .flatMap(exists -> {
                    if (exists) {
                        return this.updatePhotovoltaicSystem(solar);
                    } else {
                        logger.warn("Cannot find solar system: {}", solar.getId());
                        return this.addPhotovoltaicSystem(solar);
                    }
                });
    }

    @Override
    public Flux<DeviceConnectorInfo> getConnectors() {
        return this.getDummyDeviceConnectorInfo().cast(DeviceConnectorInfo.class);
    }

    @Override
    public Flux<DummyDeviceConnectorInfo> getDummyDeviceConnectorInfo() {
        return dummyDeviceConnectorInfoRepository.findAll();
    }

    @Override
    public Mono<DummyDeviceConnectorInfo> getDummyDeviceConnectorInfo(String photovoltaicSystemId) {
        return dummyDeviceConnectorInfoRepository.findById(photovoltaicSystemId);
    }

    @Override
    public Mono<DummyDeviceConnectorInfo> addOrUpdateDummyDeviceConnectorInfo(DummyDeviceConnectorInfo info) {
        return addDeviceConnectorInfo(info, dummyDeviceConnectorInfoRepository);
    }

    @Override
    public Mono<DummyDeviceConnectorInfo> addDummyDeviceConnectorInfo(DummyDeviceConnectorInfo info) {
        return addOrUpdateDeviceConnectorInfo(info, dummyDeviceConnectorInfoRepository);
    }

    @Override
    public Mono<Void> deleteDummyDeviceConnectorInfo(String photovoltaicSystemId) {
        return deleteDeviceConnectorInfo(photovoltaicSystemId, dummyDeviceConnectorInfoRepository, DummyDeviceConnectorInfo.class);
    }

    public <T extends DeviceConnectorInfo, U extends ReactiveMongoRepository<T, String>> Mono<T> addOrUpdateDeviceConnectorInfo(T info, U repo) {
        return getPhotovoltaicSystemById(info.getPhotovoltaicSystemId())
                .zipWith(repo.save(info))
                .doOnNext(solarSystemConnectorTuple -> {
                    T savedInfo = solarSystemConnectorTuple.getT2();
                    savedInfo.setPhotovoltaicSystem(solarSystemConnectorTuple.getT1());
                    applicationEventPublisher.publishEvent(new AddItemEvent<>(this, savedInfo));
                })
                .map(Tuple2::getT2);
    }

    public <T extends DeviceConnectorInfo, U extends  ReactiveMongoRepository<T, String>> Mono<T> addDeviceConnectorInfo(T info, U repo) {
        return getConnectors().filter(conn -> conn.getPhotovoltaicSystemId().equals(info.getPhotovoltaicSystemId()))
                .flatMap(connectorInfo -> Mono.<T>error(new InstanceAlreadyExistsException("Please remove the existing connector first!")))
                .then(this.addOrUpdateDeviceConnectorInfo(info, repo));
    }

    public <T extends DeviceConnectorInfo, U extends  ReactiveMongoRepository<T, String>> Mono<Void> deleteDeviceConnectorInfo(String solarSystemId, U repo, Class<T> clazz) {
        return repo.findById(solarSystemId).flatMap(deviceConnectorInfo ->
                repo.deleteById(deviceConnectorInfo.getPhotovoltaicSystemId())
                        .doOnSuccess(unused -> applicationEventPublisher.publishEvent(new RemoveItemEvent<T>(this, solarSystemId, clazz)))
        );
    }
}
