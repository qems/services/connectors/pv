package de.offis.e.esc.enaq.qems.solarservice.models.PhotovoltaicSystem.dtos;

import de.offis.e.esc.enaq.qems.solarservice.models.UpdateReason;
import de.offis.e.esc.enaq.qems.solarservice.models.service.ServiceConnectorInfo;
import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;

/**
 * @author Alexander Hill
 */
public class PhotovoltaicSystemRabbitDTO {
    public String id;
    public String name;
    public String description;
    public double maxElectricalPowerOutputKw;
    public String moduleName;
    public String inverterName;
    public Long stringsPerInverter;
    public Long modulesPerString;
    public Double latitude;
    public Double longitude;
    public Double altitude;
    public ServiceConnectorInfo connectorInfo;
    public UpdateReason updateReason;

    public PhotovoltaicSystemRabbitDTO() {}

    public PhotovoltaicSystemRabbitDTO(PhotovoltaicSystem solarSystem) {
        id = solarSystem.getId();
        maxElectricalPowerOutputKw = solarSystem.getOutputElectricalMax() / 1000;
        moduleName = solarSystem.getInformation().getModuleName();
        inverterName = solarSystem.getInformation().getInverterName();
        stringsPerInverter = solarSystem.getInformation().getStringsPerInverter();
        modulesPerString = solarSystem.getInformation().getModulesPerString();
        latitude = solarSystem.getLocation().getLatitude();
        longitude = solarSystem.getLocation().getLongitude();
        altitude = solarSystem.getLocation().getAltitude();
    }

    public PhotovoltaicSystemRabbitDTO(PhotovoltaicSystem solarSystem, ServiceConnectorInfo info, UpdateReason updateReason) {
        this(solarSystem);
        this.connectorInfo = info;
        this.updateReason = updateReason;
    }

    public PhotovoltaicSystemRabbitDTO(String id, ServiceConnectorInfo info, UpdateReason reason) {
        this.id = id;
        this.connectorInfo = info;
        this.updateReason = reason;
    }

    public static PhotovoltaicSystemRabbitDTO fromSolarSystem(PhotovoltaicSystem solarSystem) {
        return new PhotovoltaicSystemRabbitDTO(solarSystem);
    }

    public static PhotovoltaicSystemRabbitDTO fromSolarSystem(PhotovoltaicSystem solarSystem, ServiceConnectorInfo info, UpdateReason updateReason) {
        return new PhotovoltaicSystemRabbitDTO(solarSystem, info, updateReason);
    }

    public static PhotovoltaicSystemRabbitDTO fromId(String id, ServiceConnectorInfo info, UpdateReason updateReason) {
        return new PhotovoltaicSystemRabbitDTO(id, info, updateReason);
    }

    @Override
    public String toString() {
        return "SolarSystemRabbitDTO{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", maxElectricalPowerOutputKw=" + maxElectricalPowerOutputKw +
                ", moduleName='" + moduleName + '\'' +
                ", inverterName='" + inverterName + '\'' +
                ", stringsPerInverter=" + stringsPerInverter +
                ", modulesPerString=" + modulesPerString +
                ", connectorInfo=" + connectorInfo +
                ", updateReason=" + updateReason +
                '}';
    }
}
