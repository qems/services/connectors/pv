package de.offis.e.esc.enaq.qems.solarservice.simulation;

import de.offis.e.esc.enaq.qems.solarservice.models.devices.connectors.DummyDeviceConnectorInfo;
import de.offis.e.esc.enaq.qems.solarservice.services.PhotovoltaicSystemService;
import de.offis.e.esc.enaq.qems.solarservice.services.devices.connectors.MockPhotovoltaicSystemConnector;
import de.offis.e.esc.enaq.qems.solarservice.services.forecast.ForecastCollector;
import de.offis.e.esc.enaq.qems.solarservice.services.forecast.PvLibForecastRabbitRetriever;
import de.offis.e.esc.qems.api.entities.controls.Control;
import de.offis.e.esc.qems.api.entities.controls.Read;
import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;
import de.offis.mosaik.api.utils.generics.InputMessage;
import de.offis.mosaik.api.utils.generics.MultiModelInputSimulator;
import de.offis.mosaik.api.utils.generics.SimulationType;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import reactor.test.scheduler.VirtualTimeScheduler;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;


@Component
@Profile("simulation")
public class PhotovoltaicSystemSimulator extends MultiModelInputSimulator {

    private final Logger logger = LoggerFactory.getLogger(PhotovoltaicSystemSimulator.class);

    @Autowired
    private PhotovoltaicSystemService photovoltaicSystemService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private MockPhotovoltaicSystemConnector solarSystemConnector;

    private long secondsPerStep;

    private VirtualTimeScheduler virtualTimeScheduler;
    private Instant startTime;
    private long advancementPerStep;

    public PhotovoltaicSystemSimulator() {
        super("SolarSystem", SimulationType.TimeBased, PhotovoltaicSystem.class, PhotovoltaicSystem.class);
        this.registerFinishCreationMethod(PhotovoltaicSystem.class, this::finishPVCreation);
        this.registerStepMethod(PhotovoltaicSystem.class, this::modelStepPV);
        this.registerStepMethod(Control.Curtail.class, this::modelStepControlCurtail);
        this.registerStepMethod(Read.Prediction.class, this::modelStepGetPrediction);
        this.registerModel(Control.Curtail.class);
        this.registerModel(Control.Shutdown.class);
        this.registerModel(Read.Now.class);
        this.registerModel(Read.Prediction.class);

    }

    @Override
    public void initialize(String sid, Float timeResolution, Map<String, Object> simParams) {
        cleanSolarSystems();
        this.secondsPerStep = timeResolution.longValue(); //5 * 60; //timeResolution;
        this.advancementPerStep = (Long) simParams.getOrDefault("advancement_per_step", 10L);
        solarSystemConnector.setPublishInterval((int)(secondsPerStep * 1000));
        VirtualTimeScheduler.reset();
        this.virtualTimeScheduler = VirtualTimeScheduler.getOrSet(); // Make the VirtualTimeScheduler global

        var startTime = simParams.getOrDefault("start_time", 0L);
        if (startTime instanceof Long time) {
            this.startTime = Instant.ofEpochSecond(time);
        } else if (startTime instanceof String timeString) {
            this.startTime = ZonedDateTime.parse(timeString).toInstant();
        } else {
            throw new IllegalArgumentException("Start time must be Long or String, but is " +
                    startTime.getClass().getName()+".");
        }
        virtualTimeScheduler.advanceTimeTo(this.startTime);
    }

    @Override
    public long simulationStep(long time, Map<String, Object> map, long maxAdvance) {
        prepareGetReadData(getEntities(Read.Now.class));
        prepareGetReadPrediction(getEntities(Read.Prediction.class));
        return maxAdvance;
    }

    public Map<String, PhotovoltaicSystem> finishPVCreation(Map<String, PhotovoltaicSystem> entities) {
        for (var entity : entities.values()) {
            photovoltaicSystemService.addPhotovoltaicSystem(entity).block();
            DummyDeviceConnectorInfo info = new DummyDeviceConnectorInfo();
            info.setPhotovoltaicSystemId(entity.getId());
            info.setPhotovoltaicSystem(entity);
            // Adding the dummy connector will add the capabilities that the dummy connector provides
            // Capabilities provided are prediction, current(now) value, curtailment and shutdown
            // If they are added via mosaik, the mosaik-capabilities are used instead
            photovoltaicSystemService.addDummyDeviceConnectorInfo(info).block();
        }
        return entities;
    }
    public long modelStepPV(long time, Map<String, Map<String, InputMessage<PhotovoltaicSystem>>> input, long maxAdvance) {
        // Advance time to input time
        long targetTimeMillis = startTime.toEpochMilli() + time * this.secondsPerStep * 1000;
        logger.info("Advancing to {} ms ({})", time * this.secondsPerStep * 1000, Instant.ofEpochMilli(targetTimeMillis));
        this.virtualTimeScheduler.advanceTimeTo(Instant.ofEpochMilli(targetTimeMillis));

        // Return new time
        long nowMillis = this.virtualTimeScheduler.now(TimeUnit.MILLISECONDS);
        long now = (long) (nowMillis / 1000.0 / this.secondsPerStep);
        logger.info("now at {} steps ({} seconds)", now, nowMillis / 1000.0);
        long nextTime = time + advancementPerStep;
        logger.info("Next call at {} steps", nextTime);
        return nextTime;
    }

    public long modelStepControlCurtail(long time, Map<String, Map<String, InputMessage<Control.Curtail>>> input, long maxAdvance) {
        input.entrySet().stream().map(entry -> Map.entry(entry.getKey(), entry.getValue().values()))
                .forEach(entry -> {
                    String id = entry.getKey();
                    var inputs = entry.getValue();
                    var curtailment = inputs.stream().map(InputMessage::getInputMessage)
                            .map(Control.Curtail::getValue)
                            .flatMapToDouble(DoubleStream::of)
                            .min();
                    Double curtailDouble = null;
                    if (curtailment.isPresent()) {
                        curtailDouble = curtailment.getAsDouble();
                    }
                    getEntities(Control.Curtail.class).get(entry.getKey()).setValue(curtailDouble);
                });
        return maxAdvance;
    }

    public long modelStepGetPrediction(long time, Map<String,Map<String, InputMessage<Read.Prediction>>> input, long maxAdvance) {
        input.entrySet().stream().map(entry -> Map.entry(entry.getKey(), entry.getValue().values()))
                .forEach(entry -> {
                    String id = entry.getKey();
                    var inputs = entry.getValue();
                    var predictions = inputs.stream().map(InputMessage::getInputMessage)
                            .flatMap(prediction -> prediction.getForecast().entrySet().stream())
                            .collect(Collectors.toMap(Map.Entry::getKey, forecastEntry -> List.of(forecastEntry.getValue()), (l1, l2) -> {
                                l1.addAll(l2);
                                return l1;
                            }))
                            .entrySet().stream()
                            .collect(Collectors.toMap(longListEntry -> Long.parseLong(((String)((Object)longListEntry.getKey()))), // For some reason, the map is not parsed correctly (is Map<String, Double>). This will be corrected here
                                    fEntry -> fEntry
                                    .getValue()
                                    .stream()
                                    .mapToDouble(a -> a)
                                    .average().orElseThrow()));

                    getEntities(Read.Prediction.class).get(id).setForecast(predictions);
                });
        return maxAdvance;
    }

    public void prepareGetReadData(Map<String, Read.Now> output) {
        logger.info("Now output: {}", output);
    }

    public void prepareGetReadPrediction(Map<String, Read.Prediction> output) {
        logger.info("Prediction output: {}", output);
    }

    private void cleanSolarSystems() {
        logger.info("Run cleanSolarSystems()");
        // Clean up the database so that we have an empty start
        photovoltaicSystemService.getListOfPhotovoltaicSystems()
                .switchIfEmpty(s -> {
                    logger.info("...no solar systems found");
                    s.onComplete();
                })
                .flatMap(solarSystem -> {
                    logger.debug("...try to remove {}", solarSystem.toString());
                    return photovoltaicSystemService.removePhotovoltaicSystem(solarSystem.getId());
                })
                .blockLast();
    }

    @Override
    public void cleanup() throws Exception {
        super.cleanup();
        cleanSolarSystems();
    }
}
