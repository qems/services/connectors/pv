package de.offis.e.esc.enaq.qems.solarservice.events;

import org.springframework.context.ApplicationEvent;
import org.springframework.core.ResolvableType;
import org.springframework.core.ResolvableTypeProvider;

/**
 * @author Alexander Hill
 */
public class UpdateItemEvent<T> extends ApplicationEvent implements ResolvableTypeProvider {
    public final T item;

    /**
     * Create a new {@code ApplicationEvent}.
     *
     * @param source the object on which the event initially occurred or with
     *               which the event is associated (never {@code null})
     */
    public UpdateItemEvent(Object source, T item) {
        super(source);
        this.item = item;
    }

    @Override
    public ResolvableType getResolvableType() {
        return ResolvableType.forClassWithGenerics(
                getClass(),
                ResolvableType.forInstance(item)
        );
    }
}
