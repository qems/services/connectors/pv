package de.offis.e.esc.enaq.qems.solarservice.models.PhotovoltaicSystem.dtos;

import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;

public class PhotovoltaicSystemDto {
    //attributes
    private String id;

    @NotBlank(message = "Solar system name is mandatory")
    private String name;

    @NotBlank(message = "Solar system description is mandatory")
    private String description;

    @DecimalMin(value = "0.00", message = "Solar system output must be positive")
    private double maxElectricalPowerOutputKw;

    @Schema(example = "SunPower_SPR_210_WHT___2006_", description = "Name of the pv module. Must be matched with the sam_modules table from pvlib")
    private String moduleName;
    @Schema(example = "ABB__UNO_2_0_I_OUTD_S_US__208V_", description = "Name of the inverter. Must be matched with the cec_inverter table from pvlib")
    private String inverterName;
    @PositiveOrZero(message = "Must be provided with modulesPerString and combined power of the modules (stringsPerInverter*modulesPerString) should match maxElectricalPowerOutputKw.")
    private Long stringsPerInverter;
    @PositiveOrZero(message = "Must be provided with stringsPerInverter and combined power of the modules (stringsPerInverter*modulesPerString) should match maxElectricalPowerOutputKw.")
    private Long modulesPerString;
    @Schema(example = "52.0", description = "Latitude of the location of the pv module.")
    private Double latitude;
    @Schema(example = "8.0", description = "Longitude of the location of the pv module.")
    private Double longitude;
    @Schema(example = "0", description = "Altitude of the location of the pv module.")
    private Double altitude;

    //constructors
    public PhotovoltaicSystemDto(){
    }

    public PhotovoltaicSystemDto(PhotovoltaicSystem photovoltaicSystem){
        var information = photovoltaicSystem.getInformation();
        var location = photovoltaicSystem.getLocation();
        this.id = photovoltaicSystem.getId();
        this.name = "General Photovoltaic system " + this.id;
        this.description = "";
        this.maxElectricalPowerOutputKw = photovoltaicSystem.getOutputElectricalMax();
        this.setModuleName(information.getModuleName());
        this.setInverterName(information.getInverterName());
        this.setStringsPerInverter(information.getStringsPerInverter());
        this.setModulesPerString(information.getModulesPerString());
        this.setLatitude(location.getLatitude());
        this.setLongitude(location.getLongitude());
        this.setAltitude(location.getAltitude());
    }

    //methods
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getMaxElectricalPowerOutputKw() {
        return maxElectricalPowerOutputKw;
    }

    public void setMaxElectricalPowerOutputKw(double maxElectricalPowerOutputKw) {
        this.maxElectricalPowerOutputKw = maxElectricalPowerOutputKw;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getInverterName() {
        return inverterName;
    }

    public void setInverterName(String inverterName) {
        this.inverterName = inverterName;
    }

    public Long getStringsPerInverter() {
        return stringsPerInverter;
    }

    public void setStringsPerInverter(Long stringsPerInverter) {
        this.stringsPerInverter = stringsPerInverter;
    }

    public Long getModulesPerString() {
        return modulesPerString;
    }

    public void setModulesPerString(Long modulesPerString) {
        this.modulesPerString = modulesPerString;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }
}
