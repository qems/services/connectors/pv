package de.offis.e.esc.enaq.qems.solarservice.config;

import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Alexander Hill
 * Configuration of the default mqtt.
 */
@Configuration
public class MqttConfig {

    @Bean
    public MqttConnectOptions mqttConnectOptions(@Value("${mqtt.username:-}") String username,
                                                 @Value("${mqtt.password:-}") String password,
                                                 @Value("${mqtt.url:-}") String url) {
        MqttConnectOptions options = new MqttConnectOptions();
        if (url.isEmpty()) {
            options.setServerURIs(new String[]{url});
            if (!username.isEmpty()) {
                options.setUserName(username);
            }
            if (!password.isEmpty()) {
                options.setPassword(password.toCharArray());
            }
        }
        return options;
    }
}
