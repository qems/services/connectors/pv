package de.offis.e.esc.enaq.qems.solarservice.services.devices.connectors;

import de.offis.e.esc.enaq.qems.solarservice.events.AddItemEvent;
import de.offis.e.esc.enaq.qems.solarservice.events.RemoveItemEvent;
import de.offis.e.esc.enaq.qems.solarservice.events.UpdateItemEvent;
import de.offis.e.esc.enaq.qems.solarservice.exceptions.SolarSystemNotFoundException;
import de.offis.e.esc.enaq.qems.solarservice.facilityspecifics.PhotovoltaicSystemDummy;
import de.offis.e.esc.enaq.qems.solarservice.models.devices.connectors.DummyDeviceConnectorInfo;
import de.offis.e.esc.enaq.qems.solarservice.services.PhotovoltaicSystemConnector;
import de.offis.e.esc.enaq.qems.solarservice.services.forecast.PvLibForecastRetriever;
import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Primary;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * A connector that does not create physical connections, but uses software dummies for the Solar System management. It
 * simulates the connection layer.
 *
 * @author Christian Pieper
 */
@Service
@Primary
public class MockPhotovoltaicSystemConnector implements PhotovoltaicSystemConnector {

    private final Map<String, PhotovoltaicSystemDummy> dummies;
    private final Logger logger = LoggerFactory.getLogger(MockPhotovoltaicSystemConnector.class);
    @Value("${dummy.update-interval-ms:300000}")
    private long updateInterval;
    @Value("${dummy.publish-interval-ms:10000}")
    private long publishInterval;

    public MockPhotovoltaicSystemConnector() {
        this.dummies = new HashMap<>();
    }

    public Map<String, PhotovoltaicSystemDummy> getDummies() {
        return dummies;
    }

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;
    @Autowired
    private PvLibForecastRetriever forecastRabbitRetriever;

    @Override
    public void addPhotovoltaicSystem(PhotovoltaicSystem photovoltaicSystem) {
        logger.debug("..add PhotovoltaicSystem(id='{}') to the connector's list ", photovoltaicSystem.getId());
        PhotovoltaicSystemDummy dummy = new PhotovoltaicSystemDummy(photovoltaicSystem.getId(), photovoltaicSystem,
                true, applicationEventPublisher, getUpdateInterval(), getPublishInterval());
        dummies.put(photovoltaicSystem.getId(), dummy);
    }

    @EventListener
    public void handleAddSolarSystemEvent(AddItemEvent<DummyDeviceConnectorInfo> addSolarSystemEvent) {
        logger.debug("...received event to add SolarSystem(id='{}')", addSolarSystemEvent.item.getPhotovoltaicSystemId());
        addPhotovoltaicSystem(addSolarSystemEvent.item.getPhotovoltaicSystem());
    }

    @Override
    public void updatePhotovoltaicSystem(PhotovoltaicSystem photovoltaicSystem) {
        if (!dummies.containsKey(photovoltaicSystem.getId()))
            return;

        PhotovoltaicSystemDummy photovoltaicSystemDummy = dummies.get(photovoltaicSystem.getId());
        photovoltaicSystemDummy.setPhotovoltaicSystem(photovoltaicSystem);
    }

    @EventListener
    public void handleUpdateSolarSystemEvent(UpdateItemEvent<DummyDeviceConnectorInfo> updateSolarSystemEvent) {
        logger.debug("...received event to update SolarSystem(id='{}')", updateSolarSystemEvent.item.getPhotovoltaicSystemId());
        updatePhotovoltaicSystem(updateSolarSystemEvent.item.getPhotovoltaicSystem());
    }

    @Override
    public void removePhotovoltaicSystem(String id) {
        logger.debug("...remove SolarSystem(id='{}') from connector", id);
        PhotovoltaicSystemDummy dummyToRemove = dummies.get(id);
        if (dummyToRemove != null) {
            dummyToRemove.shutdown();
            dummies.remove(id);
        }
    }

    @EventListener
    public void handleRemoveConnectorInfoEvent(RemoveItemEvent<DummyDeviceConnectorInfo> removeSolarSystemEvent) {
        logger.debug("...received event to remove SolarSystem(id='{}')", removeSolarSystemEvent.itemId);
        removePhotovoltaicSystem(removeSolarSystemEvent.itemId);
    }

    @EventListener
    private void handleRemoveSolarSystemEvent(RemoveItemEvent<PhotovoltaicSystem> event) {
        removePhotovoltaicSystem(event.itemId);
    }

    @Override
    public void setEnergyGenerationStateOn(String solarSystemId, boolean isEnergyGenerationOn) {
        if (dummies.containsKey(solarSystemId)) {
            dummies.get(solarSystemId).setEnergyGenerationStateOn(isEnergyGenerationOn);
        } else {
            throw new SolarSystemNotFoundException(solarSystemId);
        }
    }

    @Override
    public boolean isEnergyGenerationStateOn(String photovoltaicSystemId) {
        if (dummies.containsKey(photovoltaicSystemId)) {
            return dummies.get(photovoltaicSystemId).isEnergyGenerationStateOn();
        }
        throw new SolarSystemNotFoundException(photovoltaicSystemId);
    }

    @Override
    public Set<String> getPhotovoltaicSystemIds() {
        return dummies.keySet();
    }

    @Override
    public Double setCurtailElectricalPowerOutputKw(String id, Double curtailPowerKw) {
        if (dummies.containsKey(id)) {
            return dummies.get(id).setCurtailElectricalPowerOutputKw(curtailPowerKw);
        }
        return null;
    }

    public long getUpdateInterval() {
        return updateInterval;
    }

    public void setUpdateInterval(long updateInterval) {
        this.updateInterval = updateInterval;
    }

    public long getPublishInterval() {
        return publishInterval;
    }

    public void setPublishInterval(long publishInterval) {
        this.publishInterval = publishInterval;
    }
}
