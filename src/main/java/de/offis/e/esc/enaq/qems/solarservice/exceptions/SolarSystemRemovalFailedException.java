package de.offis.e.esc.enaq.qems.solarservice.exceptions;

public class SolarSystemRemovalFailedException extends RuntimeException {
    public SolarSystemRemovalFailedException(String errorMessage) {
        super(errorMessage);
    }

    public SolarSystemRemovalFailedException(int id) {
        super(String.format("Cannot remove solar system (ID: %d)", id));
    }
}
