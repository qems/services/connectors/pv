package de.offis.e.esc.enaq.qems.solarservice.api;

import de.offis.e.esc.enaq.qems.solarservice.events.AddItemEvent;
import de.offis.e.esc.enaq.qems.solarservice.events.RemoveItemEvent;
import de.offis.e.esc.enaq.qems.solarservice.events.UpdateItemEvent;
import de.offis.e.esc.enaq.qems.solarservice.models.PhotovoltaicSystem.dtos.PhotovoltaicSystemDto;
import de.offis.e.esc.enaq.qems.solarservice.models.PhotovoltaicSystem.dtos.PhotovoltaicSystemRabbitDTO;
import de.offis.e.esc.enaq.qems.solarservice.models.UpdateReason;
import de.offis.e.esc.enaq.qems.solarservice.models.service.ServiceConnectorInfo;
import de.offis.e.esc.enaq.qems.solarservice.models.service.ServiceEvent;
import de.offis.e.esc.enaq.qems.solarservice.services.PhotovoltaicSystemService;
import de.offis.e.esc.qems.api.entities.controls.Read;
import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Publishes device events via ampq. Events are new/updated/removed batteries and new Chpp logs.
 *
 * @author Alexander Hill
 */
@Profile("!test")
@Component
public class EventAmqpPublisher {

    private final Logger logger = LoggerFactory.getLogger(EventAmqpPublisher.class);
    @Autowired
    AmqpTemplate amqpTemplate;
    @Value("${qems.solar.devices.status.routing_key}")
    String deviceStatusRoutingKey;
    @Value("${qems.solar.devices.update.routing_key}")
    String updateDeviceRoutingKey;
    @Value("${qems.solar.service.discovery.answer.routing_key}")
    String serviceEventRoutingKey;
    @Value("${qems.solar.prediction.exchange}")
    String solarPredictionExchangeName;
    @Value("${qems.solar.prediction.routing_key}")
    String solarPredictionRoutingKey;
    @Autowired
    Exchange deviceExchange;
    @Autowired
    Exchange serviceEventExchange;
    @Autowired
    ServiceConnectorInfo connectorInfo;
    @Autowired
    PhotovoltaicSystemService photovoltaicSystemService;

    @EventListener
    private void publishSolarSystemUpdate(UpdateItemEvent<PhotovoltaicSystem> updateSolarSystemEvent) {
        sendMessageAsync(PhotovoltaicSystemRabbitDTO.fromSolarSystem(
                        updateSolarSystemEvent.item, connectorInfo, UpdateReason.Update)
                , deviceExchange.getName(), updateDeviceRoutingKey);
    }

    @EventListener
    private void publishNewSolarSystemEvent(AddItemEvent<PhotovoltaicSystem> newEvent) {
        sendMessageAsync(PhotovoltaicSystemRabbitDTO.fromSolarSystem(newEvent.item, connectorInfo, UpdateReason.New)
                , deviceExchange.getName(), updateDeviceRoutingKey);
    }

    @EventListener
    private void publishRemovedSolarSystemEvent(RemoveItemEvent<PhotovoltaicSystem> removeEvent) {
        sendMessageAsync(PhotovoltaicSystemRabbitDTO.fromId(removeEvent.itemId, connectorInfo, UpdateReason.Remove)
                , deviceExchange.getName(), updateDeviceRoutingKey);
    }

    @EventListener
    private void applicationLoadedEvent(ApplicationStartedEvent startedEvent) {
        sendMessageAsync(ServiceEvent.buildServiceEvent(UpdateReason.New, connectorInfo),
                serviceEventExchange.getName(), serviceEventRoutingKey);
    }

    @EventListener
    private void applicationClosingEvent(ContextClosedEvent closedEvent) {
        sendMessageAsync(ServiceEvent.buildServiceEvent(UpdateReason.Remove, connectorInfo),
                serviceEventExchange.getName(), serviceEventRoutingKey);
    }

    @EventListener
    private void publishSolarPrediction(Read.Prediction prediction) {
        sendMessageAsync(prediction, solarPredictionExchangeName, solarPredictionRoutingKey);
    }

    private <T> void sendMessageAsync(T message, String exchange, String routingKey) {
        // Do a "fire and forget" to not crash or delay when RabbitMQ is not available (e.g. in testing situation)
        CompletableFuture.runAsync(() -> {
            try {
                amqpTemplate.convertAndSend(exchange, routingKey, message);
                logger.info("Published new AMQP message (exchange: {}, routingKey: {})", exchange, routingKey);
            } catch (Exception e) {
                logger.error("Could not send log via amqp.");
            }
        });
    }

    @RabbitListener(bindings = @QueueBinding(value = @Queue(durable = "false", autoDelete = "true"),
            exchange = @org.springframework.amqp.rabbit.annotation.Exchange(
                    value = "${qems.solar.service.discovery.exchange}",
                    type = "topic"),
            key = "${qems.solar.service.discovery.request.routing_key}"
    ))
    @SendTo("${qems.solar.service.discovery.exchange}/${qems.solar.service.discovery.answer.routing_key}")
    public ServiceEvent getDiscoverRequest(String in) {
        logger.info("Got service discovery request. Will send answer");
        return ServiceEvent.buildServiceEvent(UpdateReason.Update, connectorInfo);
    }

    @RabbitListener(bindings = @QueueBinding(value = @Queue(durable = "false", autoDelete = "true"),
            exchange = @org.springframework.amqp.rabbit.annotation.Exchange(
                    value = "${qems.solar.service.discovery.exchange}",
                    type = "topic"),
            key = {"${qems.solar.devices.pv.discovery.request.routing_key}", "${qems.solar.devices.all.discovery.request.routing_key}"}
    ))
    @SendTo("${qems.solar.service.discovery.exchange}/${qems.solar.devices.discovery.answer.routing_key}")
    public List<PhotovoltaicSystemDto> getDeviceDiscoveryRequest(String in, Message message) {
        logger.info("Got device discovery request. Will send answer");
        return photovoltaicSystemService
                .getListOfPhotovoltaicSystems()
                .filter(solarSystem -> in.isEmpty() || in.equals(solarSystem.getId()) || in.equals("null"))
                .map(PhotovoltaicSystemDto::new)
                .collectList().block();
    }

}
