package de.offis.e.esc.enaq.qems.solarservice.models.service;

import java.util.HashMap;
import java.util.Map;

/**
 * Info about how to reach the service. Can be REST and AMQP. This info will be appended to devices send via rabbitmq.
 * @author Alexander Hill
 */
public class ServiceConnectorInfo {

    public enum ConnectorType {
        REST,
        AMQP
    }

    public ConnectorType connectorType;
    public Map<String, String> properties = new HashMap<>();
}
