package de.offis.e.esc.enaq.qems.solarservice.exceptions;

public class SolarSystemLogNotFoundException extends RuntimeException {

    public SolarSystemLogNotFoundException(String id) {
        super(String.format("Cannot find log for solar system (ID: %s)", id));
    }
}
