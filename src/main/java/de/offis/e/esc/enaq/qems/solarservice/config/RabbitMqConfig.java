package de.offis.e.esc.enaq.qems.solarservice.config;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.AsyncRabbitTemplate;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Alexander Hill
 */
@Configuration
public class RabbitMqConfig {

    @Bean
    TopicExchange predictionExchange(@Value("${qems.solar.prediction.exchange}") String exchange) {
        return new TopicExchange(exchange);
    }

    @Bean
    TopicExchange deviceExchange(@Value("${qems.solar.devices.exchange}") String exchange) {
        return new TopicExchange(exchange);
    }

    @Bean
    TopicExchange serviceEventExchange(@Value("${qems.solar.service.discovery.exchange}") String exchange) {
        return new TopicExchange(exchange);
    }
    @Bean
    public Jackson2JsonMessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory, Jackson2JsonMessageConverter messageConverter) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(messageConverter);
        rabbitTemplate.setReplyTimeout(60000);
        rabbitTemplate.setReceiveTimeout(60000);
        return rabbitTemplate;
    }

    @Bean
    public AsyncRabbitTemplate asyncRabbitTemplate(RabbitTemplate rabbitTemplate, Jackson2JsonMessageConverter messageConverter) {
        rabbitTemplate.setMessageConverter(messageConverter);
        rabbitTemplate.setReplyTimeout(60000);
        rabbitTemplate.setReceiveTimeout(60000);
        return new AsyncRabbitTemplate(rabbitTemplate);
    }

}
