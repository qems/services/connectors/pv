package de.offis.e.esc.enaq.qems.solarservice.models.service;

import de.offis.e.esc.enaq.qems.solarservice.models.UpdateReason;

/**
 * @author Alexander Hill
 */
public class ServiceEvent {
    public UpdateReason updateReason;
    public ServiceConnectorInfo connectorInfo;

    public static ServiceEvent buildServiceEvent(UpdateReason updateReason, ServiceConnectorInfo connectorInfo) {
        ServiceEvent event = new ServiceEvent();
        event.updateReason = updateReason;
        event.connectorInfo = connectorInfo;
        return event;
    }
}
