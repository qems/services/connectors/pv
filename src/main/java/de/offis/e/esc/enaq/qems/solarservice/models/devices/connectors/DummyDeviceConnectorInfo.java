package de.offis.e.esc.enaq.qems.solarservice.models.devices.connectors;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Alexander Hill
 */
@Document(collection = "DummyDeviceConnectorInfo")
public class DummyDeviceConnectorInfo extends DeviceConnectorInfo {
}
