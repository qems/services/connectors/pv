package de.offis.e.esc.enaq.qems.solarservice.exceptions;

import org.springframework.http.HttpStatus;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;

/**
 * Source: https://www.baeldung.com/global-error-handler-in-a-spring-rest-api
 */
public class ApiError {
    private final ZonedDateTime dateTime;
    private HttpStatus status;
    private String message;
    private List<String> errors;

    public ApiError(HttpStatus status, String message, List<String> errors) {
        super();
        this.status = status;
        this.message = message;
        this.errors = errors;
        this.dateTime = ZonedDateTime.now(ZoneOffset.UTC);
    }

    public ApiError(HttpStatus status, String message, String error) {
        super();
        this.status = status;
        this.message = message;
        errors = Collections.singletonList(error);
        this.dateTime = ZonedDateTime.now(ZoneOffset.UTC);
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(final HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(final List<String> errors) {
        this.errors = errors;
    }

    public void setError(final String error) {
        errors = Collections.singletonList(error);
    }

    public String getTimestamp() {
        // https://stackoverflow.com/a/29626123/9390722
        // return this.dateTime.format( DateTimeFormatter.ISO_INSTANT );
        return this.dateTime.format( DateTimeFormatter.ofPattern( "yyyy-MM-dd'T'HH:mm:ssZ" ) );
    }
}
