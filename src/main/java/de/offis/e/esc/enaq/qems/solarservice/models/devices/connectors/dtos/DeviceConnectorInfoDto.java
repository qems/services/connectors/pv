package de.offis.e.esc.enaq.qems.solarservice.models.devices.connectors.dtos;

public class DeviceConnectorInfoDto {
    private String photovoltaicSystemId;

    public String getPhotovoltaicSystemId() {
        return photovoltaicSystemId;
    }

    public void setPhotovoltaicSystemId(String photovoltaicSystemId) {
        this.photovoltaicSystemId = photovoltaicSystemId;
    }
}