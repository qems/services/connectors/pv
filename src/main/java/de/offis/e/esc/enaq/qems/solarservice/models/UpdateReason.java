package de.offis.e.esc.enaq.qems.solarservice.models;

public enum UpdateReason {
    New,
    Update,
    Remove,
}
