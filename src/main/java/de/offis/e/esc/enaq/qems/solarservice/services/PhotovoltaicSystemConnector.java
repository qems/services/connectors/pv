package de.offis.e.esc.enaq.qems.solarservice.services;

import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;

import java.util.Set;

/**
 * This is the interface for classes that implement the physical connection to a SolarSystem
 *
 * @author Christian Pieper
 */
public interface PhotovoltaicSystemConnector {

    /**
     * Adds a connection to a Solar System. This could mean to establish a physical connection or to create a dummy
     * instance.
     *
     * @param photovoltaicSystem The Solar System object contains the information to connect the physical device.
     */
    void addPhotovoltaicSystem(PhotovoltaicSystem photovoltaicSystem);

    /**
     * Updates the information about a Solar System, for example the schedule. The updates are used to control the
     * Solar System.
     *
     * @param photovoltaicSystem The updated Solar System
     */
    void updatePhotovoltaicSystem(PhotovoltaicSystem photovoltaicSystem);

    /**
     * Removes a Solar System from the management and cuts the connection to the physical device.
     *
     * @param id The ID of the Solar System
     */
    void removePhotovoltaicSystem(String id);

    /**
     * Sets the energy generation state of the Solar System to on or off. Using this method, the energy generation can
     * be deactivated, e.g. for maintenance reasons.
     *  @param solarSystemId         The ID of the targeted Solar System
     * @param isEnergyGenerationOn  Describes whether the energy generation is online or offline.
     */
    void setEnergyGenerationStateOn(String solarSystemId, boolean isEnergyGenerationOn);

    /**
     * Returns the energy generation state of a specified Solar System. It indicates whet
     *
     * @param photovoltaicSystemId The ID of the targeted Solar System
     * @return true if energy generation state in on, otherwise false
     */
    boolean isEnergyGenerationStateOn(String photovoltaicSystemId);

    Set<String> getPhotovoltaicSystemIds();

    Double setCurtailElectricalPowerOutputKw(String id, Double curtailPowerKw);
}
