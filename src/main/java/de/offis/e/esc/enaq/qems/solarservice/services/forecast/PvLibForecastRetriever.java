package de.offis.e.esc.enaq.qems.solarservice.services.forecast;

import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.Map;

public interface PvLibForecastRetriever {
    Mono<Map<Long, Double>> retrievePvForecastMono(PhotovoltaicSystem solarSystem, Instant timestampStart, Instant timestampStop, Instant timestampNow);
    void setBucket(String bucket);
}
