package de.offis.e.esc.enaq.qems.solarservice.facilityspecifics;

import de.offis.e.esc.enaq.qems.solarservice.events.AddCapabilityEvent;
import de.offis.e.esc.qems.api.entities.controls.Capability;
import de.offis.e.esc.qems.api.entities.controls.Control;
import de.offis.e.esc.qems.api.entities.controls.Read;
import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.lang.reflect.InvocationTargetException;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * A dummy to simulate a Solar System, e.g. used in the MockSolarSystemConnector. Most content of this class will later
 * be handled by the real Solar Systems. Instead there might be System specific classes, which could be used as kind of
 * validators etc.
 *
 * @author Christian Pieper, Tobias Brandt
 */
public class PhotovoltaicSystemDummy {

    private final Logger logger = LoggerFactory.getLogger(PhotovoltaicSystemDummy.class);
    private final String id;
    private PhotovoltaicSystem photovoltaicSystem;
    private final ApplicationEventPublisher applicationEventPublisher;

    /* Runner function for the simulated update process */
    private Disposable updateRunner;
    /* Interval at which the internal charge of the battery is published */
    private final long updateInterval;
    private Disposable publishRunner;
    private final long publishInterval;

    private FluxSink<Instant> publishSink;
    private Disposable publishSubscription;

    public PhotovoltaicSystemDummy(String id, PhotovoltaicSystem photovoltaicSystem, boolean isEnergyGenerationStateOn,
                                   ApplicationEventPublisher applicationEventPublisher,
                                   long updateInterval, long publishInterval) {
        this.id = id;
        this.photovoltaicSystem = photovoltaicSystem;
        this.applicationEventPublisher = applicationEventPublisher;
        this.updateInterval = updateInterval;
        this.publishInterval = publishInterval;
        addCapabilities(photovoltaicSystem, isEnergyGenerationStateOn);

        createPublishSink();
        restartExecutor();
    }

    private void addCapabilities(PhotovoltaicSystem photovoltaicSystem, boolean isEnergyGenerationStateOn) {

        try {
            getOrCreate(photovoltaicSystem.getOutputElectricalPowerW(), Control.Curtail.class);
            Control.Shutdown shutdown = getOrCreate(photovoltaicSystem.getOutputElectricalPowerW(), Control.Shutdown.class);
            shutdown.setShouldShutdown(!isEnergyGenerationStateOn);

            getOrCreate(photovoltaicSystem.getOutputElectricalPowerW(), Read.Now.class);
            var prediction = getOrCreate(photovoltaicSystem.getOutputElectricalPowerW(), Read.Prediction.class);
            prediction.setForecast(new TreeMap<>());
            applicationEventPublisher.publishEvent(new AddCapabilityEvent<>(this, prediction, photovoltaicSystem));
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private <T extends Capability> T getOrCreate(Collection<Capability> capabilities, Class<T> clazz) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        var capOp = capabilities.stream().filter(clazz::isInstance).findFirst();
        if (capOp.isEmpty()) {
            T cap = clazz.getDeclaredConstructor().newInstance();
            capabilities.add(cap);
            return cap;
        }
        //noinspection unchecked
        return (T)capOp.get();
    }

    private Mono<Double> getUpdatedValue(Instant in) {
       return Mono.just(in)
                .flatMap((now) -> {
            logger.info("Running flux at {}", now);
            if(!Capability.getCapability(photovoltaicSystem.getOutputElectricalPowerW(), Control.Shutdown.class).map(Control.Shutdown::getShouldShutdown).orElse(false)) {
                logger.info("Request data from pvlib");
                final int DELTA_MINUTES = 60;
                Instant from = now.minus(DELTA_MINUTES, ChronoUnit.MINUTES);
                Instant to = now.plus(DELTA_MINUTES, ChronoUnit.MINUTES);
                return Mono.just(Capability.getCapability(photovoltaicSystem.getOutputElectricalPowerW(), Read.Prediction.class).orElseThrow().getForecast())//this.forecastRetriever.retrievePvForecastMono(solarSystem, from, to, now)
                        .map((pvOutput) -> {
                            logger.info("Got output values {}", pvOutput);
                            assert pvOutput != null;
                            Optional<Map.Entry<Long, Double>> closestOutputValue = pvOutput.entrySet().stream()//.filter(e -> e.getValue() >= 0)
                                    .min(Comparator.comparingLong(a -> Math.abs(a.getKey() - now.toEpochMilli())));
                            logger.info("Got output value {}", closestOutputValue);
                            return closestOutputValue.map(Map.Entry::getValue).filter(d -> d > 0).orElse(0.0);
                        });
            }
            return Mono.just(0.0);
        });
    }

    private void createPublishSink() {
        if (publishSubscription != null && !publishSubscription.isDisposed()) {
            publishSubscription.dispose();
            publishSink = null;
        }

        this.publishSubscription = Flux.<Instant>create(fluxSink -> this.publishSink = fluxSink)
                .subscribe((now) -> {
                    //var output = Capability.getCapability(photovoltaicSystem.getOutputElectricalPowerW(), Control.Curtail.class).map(Control.Curtail::getValue).orElse(Double.MAX_VALUE);
                    var output = Capability.getCurrentReadValue(photovoltaicSystem.getOutputElectricalPowerW());
                    logger.info("Publishing value {}", output);

                    applicationEventPublisher.publishEvent(output);
                });
    }

    private void restartExecutor() {
        assert updateRunner == null || updateRunner.isDisposed();
        updateRunner = Flux.interval(Duration.ofMillis(0), Duration.ofMillis(updateInterval))
                .map((val) -> Instant.ofEpochSecond(Schedulers.single().now(TimeUnit.SECONDS)))
                .flatMap(this::getUpdatedValue)
                .subscribe(this::updateValue);

        assert publishRunner == null || publishRunner.isDisposed();
        publishRunner = Flux.interval(Duration.ofMillis(publishInterval))
                .subscribe(a -> notifyObservers());
        // The subscription does not send an initial value. Therefore, we do that here.
        notifyObservers();
    }

    public boolean isEnergyGenerationStateOn() {
        return Capability.getCapability(photovoltaicSystem.getOutputElectricalPowerW(), Control.Shutdown.class)
                .map(shutdown -> !shutdown.getShouldShutdown())
                .orElse(true);
    }

    /**
     * Manually switch Solar System on/off, e.g. due to maintenance reasons
     *
     * @param energyGenerationStateOn State of the Solar System
     */
    public void setEnergyGenerationStateOn(boolean energyGenerationStateOn) {
        Capability.getCapability(photovoltaicSystem.getOutputElectricalPowerW(), Control.Shutdown.class).ifPresent(shutdown -> shutdown.setShouldShutdown(!energyGenerationStateOn));
        this.notifyObservers();
    }

    public void shutdown() {
        updateRunner.dispose();
        publishRunner.dispose();
        logger.debug("...Runner for PhotovoltaicSystem(id='{}') was disposed: {}", this.id, updateRunner.isDisposed());
    }

    public void updateValue(double val) {
        logger.info("...PhotovoltaicSystem(id='{}'): Updating Values", this.id);
        var curtailment = Capability.getCapability(photovoltaicSystem.getOutputElectricalPowerW(), Control.Curtail.class)
                .map(Control.Curtail::getValue)
                .orElse(Double.MAX_VALUE);
        val = Math.min(curtailment, val);
        var shutdown = Capability.getCapability(photovoltaicSystem.getOutputElectricalPowerW(), Control.Shutdown.class)
                .map(Control.Shutdown::getShouldShutdown)
                .orElse(false);
        if (shutdown) {
            val = 0.0;
        }
        Capability.getCapability(this.photovoltaicSystem.getOutputElectricalPowerW(), Read.Now.class).orElseThrow().setValue(val);

    }

    public void notifyObservers() {
        logger.debug("...PhotovoltaicSystem(id='{}'): Notifying observers", this.id);
        this.publishSink.next(Instant.ofEpochSecond(Schedulers.single().now(TimeUnit.SECONDS)));
    }

    public PhotovoltaicSystem getPhotovoltaicSystem() {
        return photovoltaicSystem;
    }

    public void setPhotovoltaicSystem(PhotovoltaicSystem photovoltaicSystem) {
        this.photovoltaicSystem = photovoltaicSystem;
        addCapabilities(photovoltaicSystem, true);
    }

    public double getLastOutputKw() {
        return Capability.getCurrentReadValue(photovoltaicSystem.getOutputElectricalPowerW()).orElse(0.0);
    }

    public Double setCurtailElectricalPowerOutputKw(Double curtailPowerW) {
        var curtail = Capability.getCapability(photovoltaicSystem.getOutputElectricalPowerW(), Control.Curtail.class).orElseThrow();
        curtail.setValue(curtailPowerW);
        return curtailPowerW;
    }
}
