package de.offis.e.esc.enaq.qems.solarservice.database;

import de.offis.e.esc.enaq.qems.solarservice.models.devices.connectors.DummyDeviceConnectorInfo;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Alexander Hill
 */
@Repository
public interface DummyDeviceConnectorInfoRepository extends ReactiveMongoRepository<DummyDeviceConnectorInfo, String> {
}
