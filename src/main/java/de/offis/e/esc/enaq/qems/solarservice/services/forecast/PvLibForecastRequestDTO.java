package de.offis.e.esc.enaq.qems.solarservice.services.forecast;

public class PvLibForecastRequestDTO {
    private final PhotovoltaicSystemDTO solarSystem;
    private final String forecastBucket;
    private final long startTimestamp;
    private final long stopTimestamp;
    private final long nowTimestamp;
    private int resolutionMinutes;

    public PvLibForecastRequestDTO(PhotovoltaicSystemDTO solarSystem, String forecastBucket, long startTimestamp, long stopTimestamp, long nowTimestamp) {
        this.solarSystem = solarSystem;
        this.forecastBucket = forecastBucket;
        this.startTimestamp = startTimestamp;
        this.stopTimestamp = stopTimestamp;
        this.nowTimestamp = nowTimestamp;
        resolutionMinutes = 15;
    }

    public PvLibForecastRequestDTO(PhotovoltaicSystemDTO solarSystem, String forecastBucket, long startTimestamp, long stopTimestamp, long nowTimestamp, int resolutionMinutes) {
        this(solarSystem, forecastBucket, startTimestamp, stopTimestamp, nowTimestamp);
        this.resolutionMinutes = resolutionMinutes;
    }

    public String getForecastBucket() {
        return forecastBucket;
    }

    public PhotovoltaicSystemDTO getSolarSystem() {
        return solarSystem;
    }

    public long getStartTimestamp() {
        return startTimestamp;
    }

    public long getStopTimestamp() {
        return stopTimestamp;
    }

    public int getResolutionMinutes() {
        return resolutionMinutes;
    }

    public long getNowTimestamp() {
        return nowTimestamp;
    }
}
