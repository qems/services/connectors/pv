package de.offis.e.esc.enaq.qems.solarservice.services.forecast;

import de.offis.e.esc.enaq.qems.solarservice.events.AddCapabilityEvent;
import de.offis.e.esc.enaq.qems.solarservice.services.PhotovoltaicSystemService;
import de.offis.e.esc.qems.api.entities.controls.Capability;
import de.offis.e.esc.qems.api.entities.controls.Read;
import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Component
@ConditionalOnProperty(name = "Collector", havingValue = "true", matchIfMissing = true)
public class ForecastCollector {
    private final ApplicationEventPublisher eventPublisher;
    private Disposable fetchDisposable;
    private final PvLibForecastRetriever pvLibForecastRetriever;

    private final PhotovoltaicSystemService photovoltaicSystemService;

    private final long fetchInterval;
    private final long forecastWindow;

    @Autowired
    public ForecastCollector(@Value("${forecast.collector.fetchIntervalSeconds:900}") long fetchInterval,
                             @Value("${forecast.collector.forecastWindowInDays:2}") long forecastWindow,
                             PhotovoltaicSystemService photovoltaicSystemService,
                             PvLibForecastRetriever pvLibForecastRetriever,
                             ApplicationEventPublisher eventPublisher) {
        this.fetchInterval = fetchInterval;
        this.forecastWindow = forecastWindow;
        this.photovoltaicSystemService = photovoltaicSystemService;
        this.pvLibForecastRetriever = pvLibForecastRetriever;
        this.eventPublisher = eventPublisher;
        createFetchSubscription();
    }

    public void createFetchSubscription() {
        if (fetchDisposable != null && !fetchDisposable.isDisposed()) {
            fetchDisposable.dispose();
        }
        fetchDisposable = Flux.interval(Duration.ofSeconds(fetchInterval))
                .flatMap(unused -> photovoltaicSystemService.getListOfPhotovoltaicSystems())
                .filter(photovoltaicSystem -> getPrediction(photovoltaicSystem).isPresent())
                .flatMap(solarSystem -> getPvPrediction(solarSystem)
                            .map(PvPrediction -> addPredictionToSolarSystem(PvPrediction, getPrediction(solarSystem).orElseThrow()))
                ).subscribe(eventPublisher::publishEvent);
    }

    private Optional<Read.Prediction> getPrediction(PhotovoltaicSystem photovoltaicSystem) {
        return Capability.getCapability(photovoltaicSystem.getOutputElectricalPowerW(), Read.Prediction.class);
    }

    public Mono<Map<Long, Double>> getPvPrediction(PhotovoltaicSystem solarSystem) {
        var now = Instant.ofEpochMilli(Schedulers.single().now(TimeUnit.MILLISECONDS)).minus(50, ChronoUnit.MINUTES);
        var window = now.plus(forecastWindow, TimeUnit.DAYS.toChronoUnit());
        return pvLibForecastRetriever.retrievePvForecastMono(solarSystem, now, window, now);
                //.map(forecastMap -> new SolarPrediction(solarSystem.getId(), forecastMap));
    }

    private Read.Prediction addPredictionToSolarSystem(Map<Long, Double> PvPrediction, Read.Prediction prediction) {
       prediction.setForecast(PvPrediction);
        return prediction;
    }

    @EventListener
    public void handleAddSolarSystemEvent(AddCapabilityEvent<Read.Prediction> capabilityAddItemEvent) {
        getPvPrediction(capabilityAddItemEvent.entity)
                .map(solarPrediction -> addPredictionToSolarSystem(solarPrediction, capabilityAddItemEvent.capability))
                .subscribe(eventPublisher::publishEvent);

    }
}
