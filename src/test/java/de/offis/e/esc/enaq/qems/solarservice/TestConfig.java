package de.offis.e.esc.enaq.qems.solarservice;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import reactor.core.scheduler.Scheduler;
import reactor.test.scheduler.VirtualTimeScheduler;

@Profile("test")
@Configuration
public class TestConfig {
    /**
     * Sets the reactor scheduler to the virtual time scheduler
     * To be applied to all subscriptions, the virtual time scheduler needs to be set as soon as possible
     * To ensure that, all necessary components should depend on this bean via @DependsOn("scheduler")
     */
    @Bean
    public Scheduler scheduler() {
        return VirtualTimeScheduler.getOrSet();
    }
}
