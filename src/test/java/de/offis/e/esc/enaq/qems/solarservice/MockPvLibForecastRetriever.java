package de.offis.e.esc.enaq.qems.solarservice;

import de.offis.e.esc.enaq.qems.solarservice.services.forecast.PvLibForecastRetriever;
import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@Component
public class MockPvLibForecastRetriever implements PvLibForecastRetriever {
    @Override
    public Mono<Map<Long, Double>> retrievePvForecastMono(PhotovoltaicSystem solarSystem, Instant timestampStart, Instant timestampStop, Instant timestampNow) {
        var data = new HashMap<Long, Double>();
        data.put(timestampStart.toEpochMilli(), solarSystem.getOutputElectricalMax());
        data.put(timestampStop.toEpochMilli(), solarSystem.getOutputElectricalMax());
        return Mono.just(data);
    }

    @Override
    public void setBucket(String bucket) {

    }
}
