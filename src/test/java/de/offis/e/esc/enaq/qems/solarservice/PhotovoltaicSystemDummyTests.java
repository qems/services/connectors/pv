package de.offis.e.esc.enaq.qems.solarservice;

import de.offis.e.esc.enaq.qems.solarservice.events.AddItemEvent;
import de.offis.e.esc.enaq.qems.solarservice.facilityspecifics.PhotovoltaicSystemDummy;
import de.offis.e.esc.enaq.qems.solarservice.services.forecast.PvLibForecastRetriever;
import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.event.ApplicationEvents;
import org.springframework.test.context.event.RecordApplicationEvents;
import reactor.test.scheduler.VirtualTimeScheduler;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * @author Alexander Hill
 */
@ActiveProfiles("test")
@SpringBootTest
@RecordApplicationEvents
@DependsOn("scheduler")
public class PhotovoltaicSystemDummyTests {
    @Autowired
    private ApplicationEvents applicationEvents;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;
    @Autowired
    private PvLibForecastRetriever forecastRetriever;

    private VirtualTimeScheduler virtualTimeScheduler;

    private List<PhotovoltaicSystemDummy> dummies = new ArrayList<>();

    private PhotovoltaicSystemDummy newSolarDummy() {
        PhotovoltaicSystemDummy dummy = new PhotovoltaicSystemDummy("Test",
                new PhotovoltaicSystem("Test", new TreeMap<>(), 20000.0, 100L, 50L, "", "", 0.0, 0.0, 0.0),
                true, applicationEventPublisher, 60000, 60000);
        dummies.add(dummy);
        return dummy;
    }

    private void cleanDummies() {
        for (PhotovoltaicSystemDummy dummy:
             dummies) {
            dummy.shutdown();
        }
        dummies.clear();
    }

    @BeforeEach
    public void before() {
        virtualTimeScheduler = VirtualTimeScheduler.getOrSet();
    }

    @AfterEach
    public void after() {
        VirtualTimeScheduler.reset();
        cleanDummies();
    }

    @Test
    @Tag("UnitTest")
    public void testDeviceCreation() {
        PhotovoltaicSystemDummy dummy = newSolarDummy();
        assert dummy.isEnergyGenerationStateOn();
    }

    @Test
    @Tag("UnitTest")
    public void testGeneration() {

        PhotovoltaicSystemDummy dummy = newSolarDummy();
        applicationEventPublisher.publishEvent(new AddItemEvent<>(this, dummy.getPhotovoltaicSystem()));
        virtualTimeScheduler.advanceTimeBy(Duration.ofMinutes(15));
        dummy.setEnergyGenerationStateOn(false);
        virtualTimeScheduler.advanceTimeBy(Duration.ofMinutes(15));
        assert !dummy.isEnergyGenerationStateOn();
        assert dummy.getLastOutputKw() == 0.0;
        dummy.setEnergyGenerationStateOn(true);
        virtualTimeScheduler.advanceTimeBy(Duration.ofMinutes(15));
        assert dummy.isEnergyGenerationStateOn();
        assert dummy.getLastOutputKw() > 0.0;
    }
}
