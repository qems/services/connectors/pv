package de.offis.e.esc.enaq.qems.solarservice;

import de.offis.e.esc.enaq.qems.solarservice.exceptions.SolarSystemNotFoundException;
import de.offis.e.esc.enaq.qems.solarservice.models.PhotovoltaicSystem.dtos.PhotovoltaicSystemDto;
import de.offis.e.esc.enaq.qems.solarservice.models.devices.connectors.DummyDeviceConnectorInfo;
import de.offis.e.esc.enaq.qems.solarservice.services.PhotovoltaicSystemService;
import de.offis.e.esc.enaq.qems.solarservice.services.devices.connectors.MockPhotovoltaicSystemConnector;
import de.offis.e.esc.qems.api.entities.controls.Capability;
import de.offis.e.esc.qems.api.entities.controls.Control;
import de.offis.e.esc.qems.api.entities.controls.Read;
import de.offis.e.esc.qems.api.entities.photovoltaic.PhotovoltaicSystem;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.ActiveProfiles;
import reactor.core.publisher.Flux;
import reactor.test.scheduler.VirtualTimeScheduler;

import java.time.Duration;
import java.util.List;
import java.util.Optional;
import java.util.TreeMap;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;

//@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@DependsOn("scheduler")
@SpringBootTest(//properties = {"spring.main.allow-bean-definition-overriding=true",
        //"spring.autoconfigure.exclude: org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration"
//}
)
public class PhotovoltaicSystemServiceApplicationTests {

    @Autowired
    private PhotovoltaicSystemService mockedPhotovoltaicSystemService;
    @Autowired
    private MockPhotovoltaicSystemConnector solarConnector;

    private VirtualTimeScheduler virtualTimeScheduler;

    @BeforeEach
    public  void inits() {
        virtualTimeScheduler = VirtualTimeScheduler.getOrSet();
    }
    @AfterEach
    public void clean() {
        cleanDb();
        VirtualTimeScheduler.reset();
    }

    private void cleanDb() {
        List<PhotovoltaicSystem> photovoltaicSystems = mockedPhotovoltaicSystemService.getListOfPhotovoltaicSystems()
                .collectList()
                .block();
        Flux.fromIterable(photovoltaicSystems)
                .flatMap(photovoltaicSystem -> mockedPhotovoltaicSystemService.removePhotovoltaicSystem(photovoltaicSystem.getId()))
                .blockLast();
    }

    @Test
    public void createEmptySolarSystemObject() {
        PhotovoltaicSystem solar = createParametrizedPhotovoltaicSystem();
        assertNotNull(solar);
    }

    @Test
    public void setSolarSystemParametersManually() {
        PhotovoltaicSystem solar = updateToPrimarySolarSystemParameters(createParametrizedPhotovoltaicSystem());
        solar.setId("1");

        assertNotNull(solar);
        assertEquals("1", solar.getId());
        checkSolarSystemParameters(solar);
    }

    private PhotovoltaicSystem updateToPrimarySolarSystemParameters(PhotovoltaicSystem solarSystem) {
        solarSystem.setOutputElectricalMax(20.4);
        return solarSystem;
    }

    private PhotovoltaicSystem createParametrizedPhotovoltaicSystem() {
        String id = null;
        double maxElectricalPowerOutputKw = 20.4;

        return new PhotovoltaicSystem(id, new TreeMap<>(), maxElectricalPowerOutputKw, 5L, 5L, "","", 0.0, 0.0, 0.0);
    }

    private void checkSolarSystemParameters(PhotovoltaicSystem addedSolarSystem) {
        assertNotNull(addedSolarSystem);
        assertEquals(20.4, addedSolarSystem.getOutputElectricalMax());
    }

    @Test
    public void createSolarSystemUsingParametrizedConstructor() {
        PhotovoltaicSystem solar = createParametrizedPhotovoltaicSystem();

        assertNotNull(solar);
        assertEquals(null, solar.getId());
        checkSolarSystemParameters(solar);
    }

    @Test
    public void addSolarSystemToDbViaService() {
        PhotovoltaicSystem solar = createParametrizedPhotovoltaicSystem();
        Optional<PhotovoltaicSystem> addedSolarSystem = mockedPhotovoltaicSystemService.addPhotovoltaicSystem(solar).blockOptional();
        assert addedSolarSystem.isPresent();
        DummyDeviceConnectorInfo info = new DummyDeviceConnectorInfo();
        info.setPhotovoltaicSystemId(addedSolarSystem.get().getId());
        mockedPhotovoltaicSystemService.addDummyDeviceConnectorInfo(info).block();

        checkSolarSystemParameters(addedSolarSystem.orElseGet(this::createParametrizedPhotovoltaicSystem));

        List<String> solarServiceIdList = mockedPhotovoltaicSystemService.getListOfPhotovoltaicSystemIds().block();
        assertEquals(1, solarServiceIdList.size());
        assertEquals(1, solarConnector.getDummies().size());
    }

    @Test
    public void deleteExistingSolarSystem() {
        PhotovoltaicSystem photovoltaicSystem = createParametrizedPhotovoltaicSystem();
        photovoltaicSystem = mockedPhotovoltaicSystemService.addPhotovoltaicSystem(photovoltaicSystem).block();

        assertNotNull(photovoltaicSystem);
        assertEquals(1, mockedPhotovoltaicSystemService.getListOfPhotovoltaicSystemIds().block().size());

        mockedPhotovoltaicSystemService.removePhotovoltaicSystem(mockedPhotovoltaicSystemService.getListOfPhotovoltaicSystemIds().block().get(0)).block();
        assertEquals(0, mockedPhotovoltaicSystemService.getListOfPhotovoltaicSystemIds().block().size());
        assertEquals(0, solarConnector.getDummies().size());

        assertThrows(SolarSystemNotFoundException.class, () -> mockedPhotovoltaicSystemService.removePhotovoltaicSystem("700").block());
    }


    @Test
    public void readSolarSystemFromDbViaService() {
        PhotovoltaicSystem newPV = createParametrizedPhotovoltaicSystem();
        PhotovoltaicSystem photovoltaicSystem = mockedPhotovoltaicSystemService.addPhotovoltaicSystem(newPV).block();
        DummyDeviceConnectorInfo info = new DummyDeviceConnectorInfo();
        info.setPhotovoltaicSystemId(photovoltaicSystem.getId());
        mockedPhotovoltaicSystemService.addDummyDeviceConnectorInfo(info).block();
        List<String> solarServiceIdList = mockedPhotovoltaicSystemService.getListOfPhotovoltaicSystemIds().block();
        assertEquals(1, solarServiceIdList.size());
        assertEquals(1, solarConnector.getDummies().size());

        Optional<PhotovoltaicSystem> solarOpt = mockedPhotovoltaicSystemService.getPhotovoltaicSystemById(solarServiceIdList.get(0)).blockOptional();
        PhotovoltaicSystem solar = solarOpt.orElseGet(this::createParametrizedPhotovoltaicSystem);

        checkSolarSystemParameters(solar);
    }

    @Test
    public void transformSolarSystemModelToDto() {
        PhotovoltaicSystem solar = createParametrizedPhotovoltaicSystem();
        PhotovoltaicSystemDto solarDto = new PhotovoltaicSystemDto(solar);

        assertNotNull(solarDto);
        assertEquals(20.4, solarDto.getMaxElectricalPowerOutputKw());
    }

    private void checkAlternativeSolarSystemParameters(PhotovoltaicSystem addedSolarSystem) {
        assertNotNull(addedSolarSystem);
        assertEquals(10.32, addedSolarSystem.getOutputElectricalMax());
    }

    @Test
    public void updateSolarSystemModel() {
        PhotovoltaicSystem solarSystem = createParametrizedPhotovoltaicSystem();
        Optional<PhotovoltaicSystem> persistentSolarOpt = mockedPhotovoltaicSystemService.addPhotovoltaicSystem(solarSystem).blockOptional();

        PhotovoltaicSystem persistentSolar = persistentSolarOpt.orElseGet(this::createParametrizedPhotovoltaicSystem);
        changeSolarSystemToAlternativeConfiguration(persistentSolar);

        mockedPhotovoltaicSystemService.updatePhotovoltaicSystem(persistentSolar).block();

        // check modified model directly
        checkAlternativeSolarSystemParameters(persistentSolar);

        // check whether modified model has also be written to the database
        Optional<PhotovoltaicSystem> updatedSolarSystemOpt = mockedPhotovoltaicSystemService.getPhotovoltaicSystemById(persistentSolar.getId())
                .blockOptional();
        PhotovoltaicSystem solar = updatedSolarSystemOpt.orElseGet(this::createParametrizedPhotovoltaicSystem);
        checkAlternativeSolarSystemParameters(solar);
    }

    private void changeSolarSystemToAlternativeConfiguration(PhotovoltaicSystem solar) {
        solar.setOutputElectricalMax(10.32);
    }

    @Test
    public void addOrUpdateSolarSystemModel() {
        // create solar system 1
        PhotovoltaicSystem solarSystem = createParametrizedPhotovoltaicSystem();
        mockedPhotovoltaicSystemService.addOrUpdatePhotovoltaicSystem(solarSystem).block();
        DummyDeviceConnectorInfo info = new DummyDeviceConnectorInfo();
        info.setPhotovoltaicSystemId(solarSystem.getId());
        info.setPhotovoltaicSystem(solarSystem);
        mockedPhotovoltaicSystemService.addDummyDeviceConnectorInfo(info).block();
        assertEquals(1, mockedPhotovoltaicSystemService.getListOfPhotovoltaicSystemIds().block().size());
        assertEquals(1, solarConnector.getDummies().size());

        // Update solar system 1
        Optional<PhotovoltaicSystem> solarSystemOpt = mockedPhotovoltaicSystemService
                .getPhotovoltaicSystemById(mockedPhotovoltaicSystemService.getListOfPhotovoltaicSystemIds().block().get(0)).blockOptional();
        solarSystemOpt.ifPresent(this::changeSolarSystemToAlternativeConfiguration);
        solarSystemOpt.ifPresent(solar -> mockedPhotovoltaicSystemService.addOrUpdatePhotovoltaicSystem(solar));
        assertEquals(1, mockedPhotovoltaicSystemService.getListOfPhotovoltaicSystemIds().block().size());
        assertEquals(1, solarConnector.getDummies().size());

        // create solar system 2
        PhotovoltaicSystem solarSystem2 = createParametrizedPhotovoltaicSystem();
        mockedPhotovoltaicSystemService.addOrUpdatePhotovoltaicSystem(solarSystem2).block();
        DummyDeviceConnectorInfo info2 = new DummyDeviceConnectorInfo();
        info2.setPhotovoltaicSystemId(solarSystem2.getId());
        info2.setPhotovoltaicSystem(solarSystem2);
        mockedPhotovoltaicSystemService.addDummyDeviceConnectorInfo(info2).block();

        assertEquals(2, mockedPhotovoltaicSystemService.getListOfPhotovoltaicSystemIds().block().size());
        assertEquals(2, solarConnector.getDummies().size());
    }

    @Test
    public void setSolarSystemEnergyGenerationState() throws InterruptedException{
        PhotovoltaicSystem solarSystem = createParametrizedPhotovoltaicSystem();
        Optional<PhotovoltaicSystem> addedSolar = mockedPhotovoltaicSystemService.addPhotovoltaicSystem(solarSystem).blockOptional();
        assertTrue(addedSolar.isPresent());
        PhotovoltaicSystem solar = addedSolar.get();
        DummyDeviceConnectorInfo info = new DummyDeviceConnectorInfo();
        info.setPhotovoltaicSystemId(addedSolar.get().getId());
        info.setPhotovoltaicSystem(addedSolar.get());
        mockedPhotovoltaicSystemService.addDummyDeviceConnectorInfo(info).block();
        // Solar system switched on
        assertFalse(Capability.getCapability(mockedPhotovoltaicSystemService.getPhotovoltaicSystemById(solar.getId()).block().getOutputElectricalPowerW(), Control.Shutdown.class).orElseThrow().getShouldShutdown());
        virtualTimeScheduler.advanceTimeBy(Duration.ofSeconds(10)); // Advance time to not wait for 10 seconds till the dummy creates the first entry
        assert mockedPhotovoltaicSystemService.getPhotovoltaicSystemById(solar.getId()).blockOptional().isPresent();
        Optional<Read.Now> latestLog = Capability.getCapability(mockedPhotovoltaicSystemService.getPhotovoltaicSystemById(solar.getId()).blockOptional().orElseThrow().getOutputElectricalPowerW(), Read.Now.class);
        assertTrue(latestLog.isPresent());
        double currentElectricalOutputKw = latestLog.get().getValue();
        assertTrue(currentElectricalOutputKw >= 0 && currentElectricalOutputKw <= solarSystem
                .getOutputElectricalMax());

        // Solar system switched off
        Capability.getCapability(mockedPhotovoltaicSystemService.getPhotovoltaicSystemById(solar.getId()).block().getOutputElectricalPowerW(), Control.Shutdown.class).orElseThrow().setShouldShutdown(true);
        virtualTimeScheduler.advanceTimeBy(Duration.ofSeconds(11)); // Advance time to not wait for 10 seconds till the dummy creates the first entry
        latestLog = Capability.getCapability(mockedPhotovoltaicSystemService.getPhotovoltaicSystemById(solar.getId()).blockOptional().orElseThrow().getOutputElectricalPowerW(), Read.Now.class);
        assertEquals(0.0, latestLog.orElseThrow().getValue());
    }

    private void updatePhotovoltaicSystem(PhotovoltaicSystem savedPhotovoltaicSystem, PhotovoltaicSystem solarSystem) {
        savedPhotovoltaicSystem.setOutputElectricalMax(solarSystem.getOutputElectricalMax());
        savedPhotovoltaicSystem.setOutputElectricalMin(solarSystem.getOutputElectricalMin());
        savedPhotovoltaicSystem.setInformation(solarSystem.getInformation());
        savedPhotovoltaicSystem.setLocation(solarSystem.getLocation());
    }

}
