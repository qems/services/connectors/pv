# Solar Service

The pv service manages photovoltaic systems and PV generation predictions.
For pv predictions and the internal pv-system-dummy for e.g. simulations, it relies on external services to provide forecasts of the PV generation to this service.

This service uses the unified QEMS-API to communicate its Entities (PhotovoltaicSystems) and their Capabilities.

This service knows two modes: _simulation_ for use with mosaik and _real-time_ for use in a
real application with either real devices or an internal dummy.

In real-time use, the used connector effectively specifies the available capabilities of a given PV system.
The Service, in real-time use, communicates the QEMS-API objects via RabbitMQ, but also exposes them via an http API.
Documentation for that API is available at `http://localhost:8080/webjars/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config`.

## Building

This simulator is a spring based java service. Build it using `./gradlew assemble`.

## Docker

It is recommended to run this service as a container.

To run this simulator, perhaps together with other simulators in containers, we suggest a docker-compose configuration:

```yaml
services:
  pv:
    image: registry.gitlab.com/qems/services/connectors/pv:latest
    environment:
      SPRING_DATA_MONGODB_USERNAME: ${qems_mongo_user}
      SPRING_DATA_MONGODB_PASSWORD: ${qems_mongo_pw}
      SPRING_RABBITMQ_PASSWORD: ${rabbitmq_password}
      SPRING_RABBITMQ_USERNAME: ${rabbitmq_user}
      SPRING_RABBITMQ_HOST: ${RABBITMQ_HOST}
      # required to activate the mosaik simulator
      SPRING_PROFILES_ACTIVE: simulation
      JAVA_OPTS: ""
```

In this configuration, the simulation mode is activated via the `SPRING_PROFILES_ACTIVE` environment variable.
If you desire, you may add JVM Options to the `JAVA_OPTS` variable, e.g. for debugging.

# Mosaik

To use this service as a mosaik simulator, activate the simulation profile by setting `SPRING_PROFILES_ACTIVE`
to `simulation` at run-time.

This service uses the qemsAPI to expose the pv systems parameters, attributes and capabilities.
The simulator continuously calculates the current power output and forecast based on the data provided by the pv-prognosis-service.

The external pv-prognosis-service in turn needs access to an InfluxDB which contains solar radiation and weather forecasts.

To explain the simulators attributes, consider the following instantiation example:

```yaml
sim_config:
  Solar:
    connect: solar:5938
simulations:
  solarSim:
    sim_name: Solar
    influxdb_bucket_forecast: ${SIMULATION_DATA_INFLUX_BUCKET} # Bucket to pull the weather and solar radiation forecasts from
    start_time: ${START_DATE_TIME_SIMULATION} # iso timestamp
    advancement_per_step: 30
devices:
  solarA:
    sim: solarSim.PhotovoltaicSystem
    maxElectricalPowerOutputW: 300000.0
    stringsPerInverter: 30
    modulesPerString: 50
    moduleName: SunPower_SPR_210_WHT___2006_ # type of module used for simulation, one of these: https://github.com/pvlib/pvlib-python/blob/4e99ddca47499715231578c3eae81ec0c4db8573/pvlib/data/sam-library-sandia-modules-2015-6-30.csv
    inverterName: Yaskawa_Solectria_Solar__SGI_300_480__480V_ # type of inverter used for simulation, one of these: https://github.com/pvlib/pvlib-python/blob/4e99ddca47499715231578c3eae81ec0c4db8573/pvlib/data/sam-library-cec-inverters-2019-03-05.csv
    latitude: 53.0
    longitude: 8.0
    altitude: 0.0
    outputElectricalPowerW:
      Now: 0.0
      Curtail:
      Shutdown:
      Prediction:
connections:
  # "Read.Now" over the current electrical output in W
  - source: solarA#/.*outputElectricalPowerW-\d\(Now\)/
    sink: <other_sim>
    attributes:
      - value
  # "Read.Prediction" over the newest prediction of electrical output in W
  - source: solarA#/.*outputElectricalPowerW-\d\(Prediction\)/
    sink: <other_sim>
    attributes:
      - value
  # "Control.Curtail" curtailing control for the output of the pv system in W
  - source: <other_sim>
    sink: solarA#/.*outputElectricalPowerW-\d\(Curtail\)/
    attributes:
      - value
  # "Control.Shutdown" capability to completely shut down the pv system
  - source: <other_sim>
    sink: solarA#/.*outputElectricalPowerW-\d\(Shutdown\)/
    attributes:
      - shouldShutdown 
```

As you can see, the simulated PV System only has one "attribute", namely outputElectricalPowerW, but allows these capabilities on it:
* Read.Now
* Read.Prediction
* Control.Curtail
* Control.Shutdown

# Real-time

The usage of the service in real-time scenarios is currently being reworked and will be documented once that is finished.
